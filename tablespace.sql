﻿-- индексы очереди
CREATE TABLESPACE ts_q_idx  OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/q_index/';

-- данные очереди
CREATE TABLESPACE ts_q_dat OWNER acc_admin LOCATION '/var/lib/postgresql/9.4/ts/q_data/';

-- индексы счета
CREATE TABLESPACE ts_acc_idx  OWNER acc_admin LOCATION '/var/lib/postgresql/9.4/ts/acc_index/';

-- данные счета
CREATE TABLESPACE ts_acc_dat   OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/acc_data/';

-- индексы транзакций
CREATE TABLESPACE ts_tr_idx   OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/tr_index/';

-- данные транзакций
CREATE TABLESPACE ts_tr_dat   OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/tr_data/';

  -- индексы транзакций
CREATE TABLESPACE ts_global_idx   OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/global_index/';

-- данные транзакций
CREATE TABLESPACE ts_global_dat   OWNER acc_admin   LOCATION '/var/lib/postgresql/9.4/ts/global_data/';