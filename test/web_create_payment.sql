﻿-- Создание счета
select  web_payment.create_account('080609ff-22d8-4b84-8dfe-937137410691'::uuid);
select  web_payment.create_account('cd1ae956-f97c-464d-9e41-f5d330d51b9d'::uuid);


select *
from account.t_account;

-- Зачисление
select web_payment.create_payment('080609ff-22d8-4b84-8dfe-937137410691'::uuid
, 20000
,20000
,'RUR'
,'test.09.01'
,'Тестовое зачисление 1');

/

-- Зачисление и отмена платежа
select web_payment.create_payment('080609ff-22d8-4b84-8dfe-937137410691'::uuid
, 50000
,50000
,'RUR'
,'test.09.02'
,'Тестовое зачисление 2');


select web_payment.abandon_payment('test.09.02','Тестовая отмена');

-- Первод денег с одного счета на другой.
select web_payment.create_transfer(
    '080609ff-22d8-4b84-8dfe-937137410691'::uuid
   ,500
   ,'cd1ae956-f97c-464d-9e41-f5d330d51b9d'
   ,500
   ,'BAL'::varchar
   ,'Тестовый перевод на другой счет'::varchar
   ,payment.get_pay_channel('account')
   ,'TRAN.09.02'::varchar
   ,NULL::json
   );