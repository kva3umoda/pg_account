﻿CREATE OR REPLACE FUNCTION web_payment.abandon_payment(
   pi_order_id  varchar
  ,pi_comment   varchar
  )
returns global.td_status.id%type
as
$BODY$
-- Отмена транзакции по зачиследнию средств
DECLARE
  l_tr_row     payment.t_transaction;
  l_tr_out_row  payment.t_transaction_out;
  l_op_row      payment.t_operation;
  l_op_tp_code varchar:='abandon_withdraw';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);

  l_delay_id  smallint;
  l_start_dt  timestamp;
  l_con_id    integer;
BEGIN
  l_con_id:=payment.get_con_by_channel(payment.get_pay_channel('webapp'));
  l_tr_out_row:=payment.get_transaction_out_row(l_con_id,pi_order_id);
  -- Блокировка транзакции для дальнешей обработки
  l_tr_row:=payment.get_transaction_row(l_tr_out_row.tr_id,true);

  if l_tr_out_row.status_id = ANY (payment.get_status('work_new','work_accepting','work_accepted','work_abandoning')) then
    l_op_row:=payment.add_operation_unique(l_tr_out_row.tr_id,l_tr_out_row.id,l_op_tp_id,payment.get_status('work_abandoning'),null::integer);
    perform payment.save_tr_param(l_tr_out_row.tr_id,l_tr_out_row.id,'abandon_comment',pi_comment);
    perform payment.set_operation_status(l_op_row.id,payment.get_status('work_abandoned'),null::integer,null::varchar,localtimestamp);
    perform payment.set_transaction_out_status(l_tr_out_row.id,payment.get_status('work_abandoned'));
    raise notice 'Отмена зачисления на счет % тразакации order_id:%',l_tr_row.acc_id,pi_order_id;
  elsif l_tr_out_row.status_id= payment.get_status('work_abandoned') then
    raise notice 'Тразакция на счет % уже на статусе отменен order_id:%',l_tr_row.acc_id,pi_order_id;
  else
    raise exception 'Не предвиденный статус "%" внешенй транзакции %',l_tr_out_row.status_id,l_tr_out_row.id;
  end if;

  return global.get_status('ok');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 2;

ALTER FUNCTION web_payment.abandon_payment( pi_order_id  varchar
                                           ,pi_comment   varchar)   OWNER TO acc_admin;
