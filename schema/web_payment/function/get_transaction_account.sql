﻿CREATE OR REPLACE FUNCTION web_payment.get_account_payments (
  in pi_user_id UUID
)
RETURNS pg_catalog.record AS
$body$
/*
  Получает информаци о счете
*/
SELECT
   acc.user_id
  ,acc.amount
  ,acc.amount_reserved
  ,acc.status_id
  ,st.code as status_code
  ,st.name as status_name
from account.t_account acc
left join account.td_status st on st.id=acc.status_id
where acc.user_id=pi_user_id;
$body$
LANGUAGE 'sql'
STABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER;


ALTER FUNCTION web_payment.get_account_info(pi_user_id  uuid)   OWNER TO acc_admin;