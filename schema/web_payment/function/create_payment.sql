﻿CREATE OR REPLACE FUNCTION web_payment.create_payment(
   pi_user_id     uuid
  ,pi_amount      integer
  ,pi_out_amount  integer
  ,pi_out_cur_id  varchar
  ,pi_order_id    varchar
  ,pi_comment     varchar
  )
returns bigint
as
$BODY$
  /* Зачисление на счет пользователя денег
     pi_user_id     uuid      -- Пользователь на счет которого идет зачисление
    ,pi_amount      integer   -- Сумма зачислинеия на счета пользователя
    ,pi_out_amount  integer   -- Cумма зачисления внешенй системы
    ,pi_out_cur_id      varchar -- Валюта из внешенй системы
    ,pi_order_id    varchar     -- Идентификатор платежа (должен быть уникальным)
    ,pi_comment     varchar     -- Комментарий к платежу
  */
DECLARE
  l_tr_out_id     bigint;
  l_tr_out_row    payment.t_transaction_out;
  l_op_row        payment.t_operation;

  l_op_tp_code  varchar:='create_withdraw';
  l_op_tp_id    integer:=payment.get_operation_type(l_op_tp_code);
  l_acc_row     account.t_account;
  l_pay_ch_id   integer;
BEGIN
  -- Блокировка счета на момент
  l_acc_row:=account.get_account_row(account.get_account_id(pi_user_id),true);
  if l_acc_row is null then
    raise exception 'Не найден счет для пользователя % ',pi_user_id;
  elsif l_acc_row.status_id!=account.get_status('active') then
    raise exception 'Счет пользователя % заблокирован, операция не возможна',pi_user_id;
  end if;
  l_pay_ch_id:=payment.get_pay_channel('webapp');
  -- Cоздание транзакции
  l_tr_out_id:=payment.create_transaction( /*pi_acc_id=>*/l_acc_row.id
                                          ,/*pi_direction=>*/1
                                          ,/*pi_amount=>*/pi_amount
                                          ,/*pi_comment=>*/pi_comment
                                          ,/*pi_pay_ch_id=>*/l_pay_ch_id
                                          ,/*pi_out_cur_id=>*/pi_out_cur_id
                                          ,/*pi_out_amont=>*/pi_out_amount
                                          ,/*pi_des_id=>*/pi_order_id
                                          ,/*pi_out_data*/null::json
                                         );
  l_tr_out_row:=payment.get_transaction_out_row(l_tr_out_id);
  l_op_row:=payment.add_operation_unique(l_tr_out_row.tr_id,l_tr_out_row.id,l_op_tp_id,payment.get_status('work_accepting'),null::integer);
  perform payment.set_operation_status(l_op_row.id,payment.get_status('work_accepted'),null::integer,pi_comment,localtimestamp);
  perform payment.set_transaction_out_status(l_tr_out_row.id,payment.get_status('work_accepted'));
  return global.get_status('ok');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 2;

ALTER FUNCTION web_payment.create_payment( pi_user_id     uuid
                                          ,pi_amount      integer
                                          ,pi_out_amount  integer
                                          ,pi_out_cur_id  varchar
                                          ,pi_order_id    varchar
                                          ,pi_comment     varchar
                                         )   OWNER TO acc_admin;