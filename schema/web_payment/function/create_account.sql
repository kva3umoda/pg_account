﻿CREATE OR REPLACE FUNCTION web_payment.create_account(
    pi_user_id  uuid
  )
returns bigint
as
$BODY$
  /*Создание счета пользователя
  result
    ok - в слачае успешного создания
    no - ничего не сделано
  */
BEGIN
  return account.create_account(pi_user_id,'BAL',0::bigint);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 2;

ALTER FUNCTION web_payment.create_account(pi_user_id  uuid)   OWNER TO acc_admin;