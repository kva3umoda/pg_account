﻿CREATE OR REPLACE FUNCTION web_payment.get_account_payments (
  in pi_user_id UUID
)
RETURNS TABLE (
  tr_id bigint
  ,amount integer
  ,acc_status_id smallint
  ,acc_status_code varchar
  ,out_status_id smallint
  ,out_status_code varchar
  ,created_dt timestamp
  ,finish_dt timestamp
  ,direction smallint
  ,pay_ch_id smallint
  ,pay_ch_code varchar
  ,pay_ch_name varchar
  ,pay_comment varchar
  ,abandon_comment varchar
) AS
$body$
/*
  Получает информаци о счете
*/
select tr.id tr_id
      ,tr.amount
      ,tr.acc_status_id
      ,st_acc.code acc_status_code
      ,tr.out_status_id
      ,st_out.code out_status_code
      ,tr.created_dt
      ,tr.finish_dt
      ,tr.direction
      ,tr.pay_ch_id
      ,pay_ch.code       as pay_ch_code
      ,pay_ch.name       as pay_ch_name
      ,prm_comment.value as pay_comment
      ,prm_ab_comment.value as abandon_comment
from payment.t_transaction tr
left join payment.td_status st_acc on st_acc.id=tr.acc_status_id
left join payment.td_status st_out on st_out.id=tr.out_status_id
left join payment.td_pay_channel pay_ch on pay_ch.id=tr.pay_ch_id
left join payment.t_transaction_param prm_comment on prm_comment.tr_id=tr.id
                                          and prm_comment.tr_out_id is null
                                          and prm_comment.param_id=global.get_param('comment')
left join payment.t_transaction_param prm_ab_comment on prm_ab_comment.tr_id=tr.id
                                          and prm_ab_comment.tr_out_id is null
                                          and prm_ab_comment.param_id=global.get_param('abandon_comment')
where tr.acc_id=account.get_account_id(pi_user_id);
$body$
LANGUAGE 'sql'
STABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER;


ALTER FUNCTION web_payment.get_account_payments(pi_user_id  uuid)   OWNER TO acc_admin;
