﻿CREATE OR REPLACE FUNCTION web_payment.create_transfer(
   pi_user_id     uuid
  ,pi_amount      integer
  ,pi_to_account  varchar
  ,pi_to_amount   integer
  ,pi_to_cur_id   varchar
  ,pi_comment     varchar
  ,pi_pay_ch_id   integer
  ,pi_order_id    varchar default null
  ,pi_out_data    json    default null
  )
returns integer
as
$BODY$
    /* Перевод денег со счета пользователя на другой
       pi_user_id     uuid      - Идентификатор пользователя
      ,pi_amount      integer   - Сумма перевод(сумма которая вычитается со счета)
      ,pi_to_account  varchar   - Куда переводим (номер карты MASTER/VISA, id_user другого пользователя, номер телефона)
      ,pi_to_amount   integer   - Сумма которая будет переводится(сумма которая зачислится во внешней системе)
      ,pi_to_cur_id   varchar   - Валюта перевода (RUR,USD,EUR ... )
      ,pi_comment     varchar   - Комментарий платежа
      ,pi_pay_ch_id   integer   - Канал платежа спровочник payment.td_pay_channel
      ,pi_order_id    varchar default null  -- Идентификатор транзацкции внешней системы(указывается на тот случай если транзацкция была инициализирована вне системы)
      ,pi_out_data    json default null     -- Доп параметры для обработки со внешенй системой
    */
DECLARE
  l_tr_out_id   bigint;
  l_tr_out_row  payment.t_transaction_out;
  l_acc_row     account.t_account;
BEGIN
  -- Блокируем счет на момент проведения операции
  l_acc_row:=account.get_account_row(account.get_account_id(pi_user_id),true);
  if l_acc_row is null then
    raise exception 'Не найден счет для пользователя % ',pi_user_id;
  elsif l_acc_row.status_id!=account.get_status('active') then
    raise exception 'Счет пользователя % заблокирован, операция не возможна',pi_user_id;
  end if;

  -- Создание транзакции
  l_tr_out_id:=payment.create_transaction( /*pi_acc_id=>*/l_acc_row.id
                                          ,/*pi_direction=>*/-1
                                          ,/*pi_amount=>*/pi_amount
                                          ,/*pi_comment=>*/pi_comment
                                          ,/*pi_pay_ch_id=>*/pi_pay_ch_id
                                          ,/*pi_out_cur_id=>*/pi_to_cur_id
                                          ,/*pi_out_amount=>*/pi_to_amount
                                          ,/*pi_des_id=>*/pi_order_id
                                          ,/*pi_out_data*/pi_out_data
                                         );
  l_tr_out_row:=payment.get_transaction_out_row(l_tr_out_id);
  -- Сохраняем назначение платежа
  perform payment.save_tr_param(l_tr_out_row.tr_id,l_tr_out_id,'to_account',pi_to_account);
  -- Резервирование денег на счете.
  perform account.create_withdraw(l_tr_out_row.tr_id);
  return global.get_status('ok');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 2;

ALTER FUNCTION web_payment.create_transfer( pi_user_id     uuid
                                           ,pi_amount      integer
                                           ,pi_to_account  varchar
                                           ,pi_to_amount   integer
                                           ,pi_to_cur_id   varchar
                                           ,pi_comment     varchar
                                           ,pi_pay_ch_id   integer
                                           ,pi_order_id    varchar
                                           ,pi_out_data    json
                                          )   OWNER TO acc_admin;


