﻿CREATE OR REPLACE FUNCTION payment.on_transaction_st_plus (
)
RETURNS trigger AS
$body$
DECLARE

BEGIN
  if new.direction!=1 then
    return new;
  end if;

  -- Проверка были ли изменения
  if TG_OP='UPDATE' then
    if  new.out_status_id=old.out_status_id
      and new.acc_status_id=old.acc_status_id then
      return new;
    end if;
  end if;

  if new.out_status_id=payment.get_status('work_accepted')
   and new.acc_status_id=payment.get_status('pay_new') then
    raise notice 'on_transaction_st_plus(id:%, acc_st(%), out_st(%)) create_payment',new.id,new.acc_status_id,new.out_status_id;
    perform account.create_payment(NEW.id);
  elsif new.out_status_id=payment.get_status('work_abandoned')
    and new.acc_status_id != ALL (payment.get_status('pay_abandoned','pay_abandoning','pay_denied'))
  then
    raise notice 'on_transaction_st_plus(id:%, acc_st(%), out_st(%)) abandon_payment',new.id,new.acc_status_id,new.out_status_id;
    perform account.abandon_payment(NEW.id);
  end if;

  return new;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;


ALTER FUNCTION payment.on_transaction_st_plus()  OWNER TO acc_admin;