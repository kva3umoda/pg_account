﻿CREATE OR REPLACE FUNCTION payment.on_transaction_out_st (
)
RETURNS trigger AS
$body$
DECLARE
  l_cnt     integer;
  l_cnt_new integer;
  l_cnt_accepting integer;
  l_cnt_accepted integer;
  l_cnt_abandoning integer;
  l_cnt_abandonend integer;
  l_cnt_denied integer;
BEGIN
  if NEW.status_id in (payment.get_status('work_new'),payment.get_status('work_changed')) then
    return new;
  end if;
  select  sum((t.status_id=payment.get_status('work_new'))::integer)
         ,sum((t.status_id in (payment.get_status('work_accepting')
                              ,payment.get_status('work_reserving')
                              ,payment.get_status('work_reserved')
                              ,payment.get_status('work_commiting')
                               ))::integer)
         ,sum((t.status_id=payment.get_status('work_accepted'))::integer)
         ,sum((t.status_id=payment.get_status('work_abandoning'))::integer)
         ,sum((t.status_id=payment.get_status('work_abandoned'))::integer)
         ,sum((t.status_id=payment.get_status('work_denied'))::integer)
         ,count(id)
  into l_cnt_new
      ,l_cnt_accepting
      ,l_cnt_accepted
      ,l_cnt_abandoning
      ,l_cnt_abandonend
      ,l_cnt_denied
      ,l_cnt
  from payment.t_transaction_out t
  where t.tr_id=NEW.tr_id
      and t.tr_out_next_id is null;

  if l_cnt=0 then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_denied')
    where id=new.tr_id;
  elsif l_cnt_accepting>0 then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_accepting')
    where id=new.tr_id;
  elsif l_cnt=l_cnt_accepted then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_accepted')
    where id=new.tr_id;
  elsif l_cnt=l_cnt_abandonend or l_cnt=(l_cnt_abandonend+l_cnt_denied) then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_abandoned')
    where id=new.tr_id;
  elsif l_cnt=l_cnt_denied then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_denied')
    where id=new.tr_id;
  elsif l_cnt_abandoning>0 then
    update payment.t_transaction
    set out_status_id=payment.get_status('work_abandoned')
    where id=new.tr_id;
  end if;

  return new;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;


ALTER FUNCTION payment.on_transaction_out_st()  OWNER TO acc_admin;