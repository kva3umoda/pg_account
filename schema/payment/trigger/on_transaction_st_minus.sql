﻿CREATE OR REPLACE FUNCTION payment.on_transaction_st_minus (
)
RETURNS trigger AS
$body$
DECLARE

BEGIN
  if new.direction::integer != -1 then
    return new;
  end if;

  -- Проверка были ли изменения
  if TG_OP='UPDATE' then
    if  new.out_status_id=old.out_status_id
      and new.acc_status_id=old.acc_status_id then
      return new;
    end if;
  end if;

  if new.out_status_id = payment.get_status('work_accepted')
   and new.acc_status_id = payment.get_status('pay_reserved') then
    raise notice 'on_transaction_st_minus(id:%, acc_st(%), out_st(%)) confirm_withdraw',new.id,new.acc_status_id,new.out_status_id;
    perform account.confirm_withdraw(new.id);
  elsif new.out_status_id = payment.get_status('work_abandoned')
   and new.acc_status_id != all (payment.get_status('pay_abandoning','pay_abandoned')) then
    raise notice 'on_transaction_st_minus(id:%, acc_st(%), out_st(%)) abandon_withdraw',new.id,new.acc_status_id,new.out_status_id;
    perform account.abandon_withdraw(new.id);
  elsif new.out_status_id = payment.get_status('work_denied') then
    if new.acc_status_id = ANY (payment.get_status('pay_new','pay_accepting','pay_reserving','pay_reserverd')) then
      raise notice 'on_transaction_st_minus(id:%, acc_st(%), out_st(%)) abandon_withdraw',new.id,new.acc_status_id,new.out_status_id;
      perform account.abandon_withdraw(new.id);
    end if;
  elsif new.acc_status_id=payment.get_status('pay_reserved')
    and new.out_status_id=payment.get_status('work_new') then
    raise notice 'on_transaction_st_minus(id:%, acc_st(%), out_st(%)) init_transaction_out_create',new.id,new.acc_status_id,new.out_status_id;
    perform payment.init_transaction_out_create(new.id);
  elsif new.acc_status_id=payment.get_status('pay_abandoning') then
    --raise notice 'on_transaction_st_minus(id:%, acc_st(%), out_st(%)) null',new.id,new.acc_status_id,new.out_status_id;
    --init_transaction_out_abandon(new.tr_id);
    null;
  end if;

  return new;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;


ALTER FUNCTION payment.on_transaction_st_minus()  OWNER TO acc_admin;