﻿-- Локальный драйвера account
insert into payment.td_driver(code,name,class_name,is_async)
    values('account','Локальная система аккаунта',null,false);

insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save)
  values(payment.get_driver('account'),payment.get_operation_type('create_payment'),'','account.create_payment_out');

insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save)
  values(payment.get_driver('account'),payment.get_operation_type('abandon_payment'),'','account.abandon_payment_out');

insert into payment.td_connection(name,driver_id,queue_code,is_active)
    values('Локальный счет баллов',payment.get_driver('account'),'account',true);

insert into payment.td_connection(name,driver_id,queue_code,direction,is_active)
  values('Входящие соединение ACCOUNT',payment.get_driver('account'),'account',0,true);

insert into payment.t_con_pay_ch(con_id,pay_ch_id)
  values(payment.get_con_by_queue('account'),payment.get_pay_channel('account'));

-- Драйвер для обработки с web
insert into payment.td_driver(code,name,class_name,is_async)
    values('web_app','Драйвер web приложения',null,false);

insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save,class_name)
  values(payment.get_driver('web_app'),payment.get_operation_type('create_withdraw'),null,'web_payment.create_payment',null);
insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save,class_name)
  values(payment.get_driver('web_app'),payment.get_operation_type('abandon_withdraw'),null,'web_payment.abandon_payment',null);

insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save,class_name,is_manual)
  values(payment.get_driver('web_app'),payment.get_operation_type('create_payment'),null,'web_payment.on_create_payment',null,true);
insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save,class_name,is_manual)
  values(payment.get_driver('web_app'),payment.get_operation_type('abandon_payment'),null,'web_payment.on_abandon_payment',null,true);

insert into payment.td_connection(name,driver_id,queue_code,is_active)
    values('Соединени с WEB приложения',payment.get_driver('web_app'),'web_app',true);

insert into payment.t_con_pay_ch(con_id,pay_ch_id)
  values(payment.get_con_by_queue('web_app'),payment.get_pay_channel('webapp'));

