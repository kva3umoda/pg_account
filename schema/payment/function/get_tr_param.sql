﻿CREATE OR REPLACE FUNCTION payment.get_tr_param(
   in pi_tr_id bigint
  ,in pi_prm_code varchar
)
RETURNS varchar
AS
$body$
  select value
  from payment.t_transaction_param as val
  where val.tr_id=pi_tr_id
      and val.tr_out_id is null
      and val.param_id=global.get_param(pi_prm_code);
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_tr_param(bigint,varchar)  OWNER TO acc_admin;

CREATE OR REPLACE FUNCTION payment.get_tr_param(
   in pi_tr_id bigint
  ,in pt_tr_out_id bigint
  ,in pi_prm_code varchar
)
RETURNS varchar
AS
$body$
  select value
  from payment.t_transaction_param as val
  where val.tr_id=pi_tr_id
      and val.tr_out_id=pt_tr_out_id
      and val.param_id=global.get_param(pi_prm_code);
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 30;

ALTER FUNCTION payment.get_tr_param(bigint,bigint,varchar)  OWNER TO acc_admin;
