﻿CREATE OR REPLACE FUNCTION payment.get_status (
  in pi_code payment.td_status.code%type
)
RETURNS payment.td_status.id%type
AS
$body$
declare
  l_id  payment.td_status.id%type;
begin
  select id
  into l_id
  from payment.td_status
  where code=lower(pi_code);
  if NOT FOUND then
    raise exception 'Не верно указан код "%" статуса в payment',lower(pi_code);
  end if;
  return l_id;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_status(payment.td_status.code%type)  OWNER TO acc_admin;


CREATE OR REPLACE FUNCTION payment.get_status (
  variadic pi_code_list varchar []
)
RETURNS smallint [] AS
$body$
DECLARE
  l_id_list  smallint[];
begin
  select array_agg(id)
  into l_id_list
  from payment.td_status
  where code = ANY (pi_code_list);
  if NOT FOUND then
    raise exception 'Не верно указаны коды "%" статуса в payment',pi_code_list;
  elsif array_length(pi_code_list,1)!=array_length(l_id_list,1) then
    raise exception 'Не верно указан код в списке "%"',pi_code_list;
  end if;
  return l_id_list;
END;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 10;

ALTER FUNCTION payment.get_status(pi_code_list varchar [])  OWNER TO acc_admin;