﻿CREATE OR REPLACE FUNCTION payment.save_tr_param(
   pi_tr_id     bigint
  ,pi_prm_code  varchar
  ,pi_prm_value varchar
)
RETURNS void AS
$body$
DECLARE

BEGIN
  with prm_val as (select pi_tr_id  as tr_id
                         ,null      as tr_out_id
                         ,global.get_param(pi_prm_code) as param_id
                         ,pi_prm_value as value
                  )
      ,prm_upd as (UPDATE payment.t_transaction_param as tr_prm
                   SET value = prm_val.value
                   FROM prm_val
                   WHERE tr_prm.tr_out_id is null
                        and tr_prm.tr_id=prm_val.tr_id
                        and tr_prm.param_id=prm_val.param_id
                   returning tr_prm.*
                            )
  insert into payment.t_transaction_param(tr_id,param_id,value)
  select t1.tr_id,t1.param_id,t1.value
  from prm_val as t1
  where (tr_id,param_id) not in (select tr_id,param_id from prm_upd);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;

ALTER FUNCTION payment.save_tr_param(bigint, varchar,varchar) OWNER TO acc_admin;


CREATE OR REPLACE FUNCTION payment.save_tr_param(
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_prm_code  varchar
  ,pi_prm_value varchar
)
RETURNS void AS
$body$
DECLARE

BEGIN
  with prm_val as (select pi_tr_id  as tr_id
                         ,pi_tr_out_id  as tr_out_id
                         ,global.get_param(pi_prm_code) as param_id
                         ,pi_prm_value as value
                 )
      ,prm_upd as (UPDATE payment.t_transaction_param as tr_prm
                   SET value = prm_val.value
                   FROM prm_val
                   WHERE tr_prm.tr_out_id=prm_val.tr_out_id
                        and tr_prm.tr_id=prm_val.tr_id
                        and tr_prm.param_id=prm_val.param_id
                   returning tr_prm.*
                            )
  insert into payment.t_transaction_param(tr_id,tr_out_id,param_id,value)
  select t1.tr_id,t1.tr_out_id,t1.param_id,t1.value
  from prm_val as t1
  where (tr_id,tr_out_id,param_id) not in (select tr_id,tr_out_id,param_id from prm_upd);
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;

ALTER FUNCTION payment.save_tr_param(bigint,bigint, varchar,varchar) OWNER TO acc_admin;
