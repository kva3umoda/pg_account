﻿CREATE OR REPLACE FUNCTION payment.set_transaction_status (
  pi_tr_id bigint,
  pi_acc_st_id integer
)
RETURNS void AS
$body$
DECLARE
  l_finish_time timestamp;
  l_row_count integer;
BEGIN
  if pi_acc_st_id in (payment.get_status('pay_accepted')
                     ,payment.get_status('pay_abandoned')
                     ,payment.get_status('pay_denied')) then
    l_finish_time:=current_timestamp;
  end if;
  update payment.t_transaction
  set acc_status_id=pi_acc_st_id
     ,finish_dt=COALESCE(l_finish_time,finish_dt)
  where id=pi_tr_id;
  GET DIAGNOSTICS l_row_count = ROW_COUNT;
  if l_row_count=0 then
    raise exception 'Не найдена транзакция счета %',pi_tr_id;
  end if;
  return;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.set_transaction_status(bigint, integer) OWNER TO acc_admin;