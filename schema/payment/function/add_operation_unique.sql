﻿CREATE OR REPLACE FUNCTION payment.add_operation_unique (
  pi_tr_id bigint,
  pi_op_tp_id integer,
  pi_st_id smallint,
  pi_event_id integer
)
RETURNS payment.t_operation AS
$body$
DECLARE
  l_op_row payment.t_operation;
  l_req_time timestamp;
BEGIN
  select *
  into l_op_row
  from payment.t_operation
  where tr_id=pi_tr_id and type_id=pi_op_tp_id and tr_out_id is null
  limit 1;
  if l_op_row is null then
    l_req_time:=current_timestamp;
    insert into payment.t_operation(id,tr_id,tr_out_id
              ,type_id,status_id,event_id
              ,created_dt,updated_dt
              ,req_status_id,req_note,req_dt)
      values(nextval('payment.seq_operation'),pi_tr_id,null
              ,pi_op_tp_id,pi_st_id,pi_event_id
              ,l_req_time,l_req_time
              ,null,null,null)
    returning * into l_op_row;
  end if;
  return l_op_row;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.add_operation_unique(bigint, integer, smallint, integer) OWNER TO acc_admin;


CREATE OR REPLACE FUNCTION payment.add_operation_unique (
  pi_tr_id bigint,
  pi_tr_out_id bigint,
  pi_op_tp_id integer,
  pi_st_id smallint,
  pi_event_id integer
)
RETURNS payment.t_operation AS
$body$
DECLARE
  l_op_row payment.t_operation;
  l_req_time timestamp;
BEGIN
  select *
  into l_op_row
  from payment.t_operation
  where tr_id=pi_tr_id and type_id=pi_op_tp_id and tr_out_id=pi_tr_out_id
  limit 1;
  if l_op_row is null then
    l_req_time:=current_timestamp;
    insert into payment.t_operation(id,tr_id,tr_out_id
              ,type_id,status_id,event_id
              ,created_dt,updated_dt
              ,req_status_id,req_note,req_dt)
      values(nextval('payment.seq_operation'),pi_tr_id,pi_tr_out_id
              ,pi_op_tp_id,pi_st_id,pi_event_id
              ,l_req_time,l_req_time
              ,null,null,null)
    returning * into l_op_row;
  end if;
  return l_op_row;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.add_operation_unique(bigint, bigint, integer, smallint, integer) OWNER TO acc_admin;
