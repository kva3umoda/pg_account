﻿CREATE OR REPLACE FUNCTION payment.get_driver (
  in pi_code varchar
)
RETURNS integer
AS
$body$
  select id
  from payment.td_driver
  where code=lower(pi_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_driver(character varying)  OWNER TO acc_admin;