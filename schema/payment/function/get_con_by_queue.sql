﻿CREATE OR REPLACE FUNCTION payment.get_con_by_queue(
  in pi_queue_code varchar
)
RETURNS integer
AS
$body$
    -- Получение идентификатор драйвера по очереди
  select con.id
  from payment.td_connection as con
  where con.queue_code=lower(pi_queue_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_con_by_queue(character varying)  OWNER TO acc_admin;

