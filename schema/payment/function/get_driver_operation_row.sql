﻿CREATE OR REPLACE FUNCTION payment.get_driver_operation_row
(
  pi_dr_id   integer,
  pi_op_code varchar
)
RETURNS payment.t_driver_operation
AS
$body$
declare
  l_row payment.t_driver_operation;
BEGIN
  select *
  into l_row
  from payment.t_driver_operation t
  where t.driver_id=pi_dr_id
    and t.op_type_id=payment.get_operation_type(pi_op_code);
  if NOT FOUND then
    raise exception 'Для драйвера id:% не существует тип операции "%"',pi_dr_id,pi_op_code;
  end if;
  return l_row;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION payment.get_driver_operation_row(integer, varchar) OWNER TO acc_admin;