﻿CREATE OR REPLACE FUNCTION payment.get_transaction_out_row(
  pi_tr_out_id bigint
)
RETURNS payment.t_transaction_out AS
$body$
declare
  l_row payment.t_transaction_out;
BEGIN
  select t.*
  into l_row
  from payment.t_transaction_out t
  where t.id=pi_tr_out_id;
  return l_row;
END;
$body$
LANGUAGE 'plpgsql'
STABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION payment.get_transaction_out_row(bigint) OWNER TO acc_admin;


CREATE OR REPLACE FUNCTION payment.get_transaction_out_row(
   pi_con_id    payment.t_transaction_out.con_id%type
  ,pi_tr_des_id payment.t_transaction_out.tr_des_id%type
)
RETURNS payment.t_transaction_out AS
$body$
declare
  l_row payment.t_transaction_out;
BEGIN
  select tr.*
  into l_row
  from payment.t_transaction_out tr
  where tr.con_id=pi_con_id and tr.tr_des_id=pi_tr_des_id;
  return l_row;
END;
$body$
LANGUAGE 'plpgsql'
STABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION payment.get_transaction_out_row(payment.t_transaction_out.con_id%type,payment.t_transaction_out.tr_des_id%type) OWNER TO acc_admin;
