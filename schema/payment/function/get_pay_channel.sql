﻿CREATE OR REPLACE FUNCTION payment.get_pay_channel (
  in pi_code varchar
)
RETURNS integer
AS
$body$
  select id
  from payment.td_pay_channel
  where code=lower(pi_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_pay_channel(character varying)  OWNER TO acc_admin;