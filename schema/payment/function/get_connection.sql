﻿CREATE OR REPLACE FUNCTION payment.get_connection (
  in pi_queue_code varchar
)
RETURNS integer
AS
$body$
    -- Получение идентификатор содениия по коду очереди
  select id
  from payment.td_connection
  where queue_code=lower(pi_queue_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_connection(character varying)  OWNER TO acc_admin;


