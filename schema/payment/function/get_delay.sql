﻿CREATE OR REPLACE FUNCTION payment.get_delay (
  in pi_code varchar
)
RETURNS smallint
AS
$body$
  select id
  from payment.td_delay
  where code=lower(pi_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_delay(character varying)  OWNER TO acc_admin;