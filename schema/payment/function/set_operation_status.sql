﻿CREATE OR REPLACE FUNCTION payment.set_operation_status (
  pi_op_id bigint,
  pi_st_id smallint,
  pi_req_st_id integer,
  pi_req_note  varchar,
  pi_req_dt    timestamp
)
RETURNS void AS
$body$
DECLARE
  l_op_row payment.t_operation;
  l_req_time timestamp;
  l_row_count integer;
BEGIN
  l_req_time:=current_timestamp;
  update payment.t_operation
  set status_id=pi_st_id
     ,updated_dt=l_req_time
     ,req_status_id=pi_req_st_id
     ,req_note=pi_req_note
     ,req_dt=pi_req_dt
  where id=pi_op_id;
  GET DIAGNOSTICS l_row_count = ROW_COUNT;
  if l_row_count=0 then
    raise exception 'Не найдена операция транзакции %',pi_op_id;
  end if;
  return;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.set_operation_status(bigint, smallint, integer, varchar, timestamp) OWNER TO acc_admin;
