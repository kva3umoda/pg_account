﻿CREATE OR REPLACE FUNCTION payment.create_transaction(
   pi_acc_id       uuid
  ,pi_direction    integer
  ,pi_amount       integer
  ,pi_comment      varchar
  ,pi_pay_ch_id    integer
  ,pi_out_cur_id   varchar
  ,pi_out_amount   integer
  ,pi_out_order_id varchar
  ,pi_out_data     json default null
  )
  RETURNS bigint AS
$BODY$
-- Создает транзакцию и возвращает идентификатор внешенй транзакции
DECLARE
  l_tr_id     bigint;
  l_tr_out_id bigint;
  l_delay_id  smallint;
  l_start_dt  timestamp;
  l_con_id    integer;
BEGIN
  l_start_dt:=localtimestamp;
  case pi_direction
      when -1 then l_delay_id=payment.get_delay('two');
      when 1  then l_delay_id=payment.get_delay('one');
  end case;
  -- Создание транзакции
  l_tr_id:=nextval('payment.seq_transaction');
  insert into payment.t_transaction(id, acc_id, amount, delay_id, acc_status_id, out_status_id, created_dt, direction, pay_ch_id)
      values(l_tr_id,pi_acc_id,pi_amount,l_delay_id
              ,payment.get_status('pay_new'),payment.get_status('work_new')
              ,l_start_dt,pi_direction,pi_pay_ch_id);
  perform payment.save_tr_param(l_tr_id,'comment',pi_comment);

  -- Создание внешеней транзакции
  l_tr_out_id:=nextval('payment.seq_transaction');

  l_con_id:=payment.get_con_by_channel(pi_pay_ch_id);
  insert into payment.t_transaction_out(id,tr_id,tr_des_id,con_id,delay_id,status_id,cur_id,amount,created_dt)
      values(l_tr_out_id,l_tr_id,pi_out_order_id,l_con_id,payment.get_delay('one'),payment.get_status('work_new')::smallint,upper(pi_out_cur_id),pi_out_amount,l_start_dt);

  perform payment.save_tr_params(l_tr_id,l_tr_out_id,pi_out_data);

  return l_tr_out_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 2;

ALTER FUNCTION payment.create_transaction( pi_acc_id       uuid
                                          ,pi_direction    integer
                                          ,pi_amount       integer
                                          ,pi_comment      varchar
                                          ,pi_pay_ch_id    integer
                                          ,pi_out_cur_id   varchar
                                          ,pi_out_amount   integer
                                          ,pi_out_order_id varchar
                                          ,pi_out_data     json
                                           ) OWNER TO acc_admin;
