﻿CREATE OR REPLACE FUNCTION payment.get_operation_type (
  in pi_code payment.td_operation_type.code%type
)
RETURNS payment.td_operation_type.id%type
AS
$body$
declare
  l_id  payment.td_operation_type.id%type;
begin
  select id
  into l_id
  from payment.td_operation_type
  where code=lower(pi_code);
  if NOT FOUND then
    raise exception 'Не верно указан код "%" операции в payment',lower(pi_code);
  end if;
  return l_id;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION payment.get_operation_type(payment.td_operation_type.code%type)  OWNER TO acc_admin;