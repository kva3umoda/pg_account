﻿CREATE OR REPLACE FUNCTION payment.get_con_params(
  in pi_con_id integer
)
RETURNS json
AS
$body$
  select json_object_agg(prm.code
          ,(case prm.data_type
              when 'N' then to_json(val.value::integer)
              when 'B' then to_json(val.value::boolean)
              when 'J' then val.value::json
              else to_json(val.value)
            end)
           ) json
  from (select con_prm.param_id
              ,con_prm.value
        from payment.t_con_param as con_prm
        where con_prm.con_id=pi_con_id
          union all
        select dr_prm.param_id
              ,dr_prm.value
        from payment.td_connection as con
        inner join payment.t_driver_param as dr_prm on dr_prm.driver_id=con.driver_id
        where con.id=pi_con_id
            and not exists(select 1 from payment.t_con_param as t
                           where con.id=t.con_id and dr_prm.param_id=t.param_id
                          )
         ) as val
  left join global.td_param as prm on prm.id=val.param_id;
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_con_params(integer)  OWNER TO acc_admin;


