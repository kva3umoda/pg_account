﻿CREATE OR REPLACE FUNCTION payment.get_operation_row (
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_type      integer
)
RETURNS payment.t_operation AS
$body$
  select *
  from payment.t_operation as op
  where op.tr_id=pi_tr_id
       and op.tr_out_id=pi_tr_out_id
       and op.type_id=pi_type;
$body$
LANGUAGE 'sql'
STABLE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER;


ALTER FUNCTION payment.get_operation_row(bigint,bigint,integer)  OWNER TO acc_admin;

CREATE OR REPLACE FUNCTION payment.get_operation_row (
   pi_tr_id bigint
  ,pi_type integer
)
RETURNS payment.t_operation AS
$body$
  select *
  from payment.t_operation as op
  where op.tr_id=pi_tr_id
       and op.type_id=pi_type;
$body$
LANGUAGE 'sql'
STABLE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER;


ALTER FUNCTION payment.get_operation_row(bigint,integer)  OWNER TO acc_admin;