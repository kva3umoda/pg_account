﻿CREATE OR REPLACE FUNCTION payment.get_transaction_row(
  pi_tr_id bigint,
  pi_is_lock boolean
)
RETURNS payment.t_transaction AS
$body$
declare
  l_row payment.t_transaction;
BEGIN
    if pi_is_lock then
        select t.*
        into l_row
        from payment.t_transaction t
        where t.id=pi_tr_id
        for update;
    else
        select t.*
        into l_row
        from payment.t_transaction t
        where t.id=pi_tr_id;
    end if;
    return l_row;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION payment.get_transaction_row(bigint, boolean) OWNER TO acc_admin;
