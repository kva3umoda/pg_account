﻿CREATE OR REPLACE FUNCTION payment.get_con_by_channel(
  in pi_pay_ch_id integer
)
RETURNS integer
AS
$body$
    -- Получение идентификатор соединения.
  select con_id
  from  payment.t_con_pay_ch
  where pay_ch_id=pi_pay_ch_id
  limit 1;
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_con_by_channel(integer)  OWNER TO acc_admin;

