﻿CREATE OR REPLACE FUNCTION payment.init_transaction_out_create
(
  pi_tr_id bigint
)
RETURNS void AS
$body$
DECLARE
  ir_row record;
  l_row_count integer;
  l_dr_op_row payment.t_driver_operation;
BEGIN
  for ir_row in select tr_out.id,tr_out.status_id,tr_out.con_id
                       ,con.queue_code
                       ,con.driver_id
                       ,dr.code as driver_code
                 from payment.t_transaction_out tr_out
                 left join payment.td_connection con on con.id=tr_out.con_id
                 left join payment.td_driver dr  on dr.id=con.driver_id
                 where tr_out.tr_id =pi_tr_id
                      and status_id=payment.get_status('work_new')

  loop
    if ir_row.driver_code='account' then
      perform account.create_payment_out(pi_tr_id,ir_row.id,null::json);
    else
      l_dr_op_row:=payment.get_driver_operation_row(ir_row.driver_id,'create_payment');
      if not l_dr_op_row.is_manual then
        perform queue.add_event(ir_row.queue_code,'payment'
                                ,format('{"tr_id":%s,"tr_out_id":%s,"operation_type":"%s"}'
                                          ,pi_tr_id
                                          ,ir_row.id
                                          ,'create_payment'
                                        )
                                );
      end if;
    end if;
  end loop;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.init_transaction_out_create(bigint) OWNER TO acc_admin;