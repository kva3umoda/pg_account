﻿CREATE OR REPLACE FUNCTION payment.get_tr_params(
  in pi_tr_id bigint
)
RETURNS json
AS
$body$
  select json_object_agg(prm.code
          ,(case prm.data_type
              when 'N' then to_json(val.value::integer)
              when 'B' then to_json(val.value::boolean)
              when 'J' then val.value::json
              else to_json(val.value)
            end)
           ) json
  from payment.t_transaction_param as val
  left join global.td_param as prm on prm.id=val.param_id
  where val.tr_id=pi_tr_id and val.tr_out_id is null
  group by val.tr_id,val.tr_out_id;

$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 30;

ALTER FUNCTION payment.get_tr_params(bigint)  OWNER TO acc_admin;

CREATE OR REPLACE FUNCTION payment.get_tr_params(
  in pi_tr_id bigint,
  in pt_tr_out_id bigint
)
RETURNS json
AS
$body$
  select json_object_agg(prm.code
          ,(case prm.data_type
              when 'N' then to_json(val.value::integer)
              when 'B' then to_json(val.value::boolean)
              when 'J' then val.value::json
              else to_json(val.value)
            end)
           ) json
  from payment.t_transaction_param as val
  left join global.td_param as prm on prm.id=val.param_id
  where val.tr_id=pi_tr_id and val.tr_out_id=pt_tr_out_id
  group by val.tr_id,val.tr_out_id;

$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 30;

ALTER FUNCTION payment.get_tr_params(bigint,bigint)  OWNER TO acc_admin;
