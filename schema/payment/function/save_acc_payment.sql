﻿CREATE OR REPLACE FUNCTION payment.save_acc_payment(
   in pi_tr_id bigint
)
RETURNS INTEGER
AS
$body$
-- зачисление средств на счет
DECLARE

BEGIN
   return global.get_status('ok');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER;


ALTER FUNCTION payment.save_acc_payment(bigint)  OWNER TO acc_admin;