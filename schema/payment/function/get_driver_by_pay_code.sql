﻿CREATE OR REPLACE FUNCTION payment.get_driver_by_pay_code(
  in pi_pay_code varchar
)
RETURNS integer
AS
$body$
    -- Получение идентификатор драйвера по очереди
  select dr.driver_id
  from payment.td_pay_channel as pc
  inner join   payment.t_driver_pay_ch as dr on dr.pay_ch_id=pc.id
  where pc.code=lower(pi_pay_code);
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION payment.get_driver_by_pay_code(character varying)  OWNER TO acc_admin;

