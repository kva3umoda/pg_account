﻿--drop table payment.td_connection;
CREATE TABLE payment.td_connection
(
   id         serial
  ,name 		  text  not null
  ,driver_id 	integer
  ,queue_code text
  ,direction 	smallint default 1 not null
  ,is_active  boolean  default true not null
  ,constraint pk_conn primary key(id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

alter table payment.td_connection OWNER TO acc_admin;

comment on table payment.td_connection is 'Таблица соединений';
comment on column payment.td_connection.id is 'Идентификатор соединения';
comment on column payment.td_connection.name is 'Наименование очереди';
comment on column payment.td_connection.driver_id is 'Таблица с событиями';
comment on column payment.td_connection.queue_code is 'Уникальный код очереди';
comment on column payment.td_connection.is_active is 'Признак активности соединения';
comment on column payment.td_connection.direction is 'Направление 0- входящие; 1-исходящие';

alter table payment.td_connection add constraint fk_conn_dr foreign key(driver_id) references payment.td_driver(id);
create index ix_conn_dr on payment.td_connection(driver_id) tablespace ts_tr_idx;
create unique index ux_conn_queue on payment.td_connection(lower(queue_code)) tablespace ts_tr_idx;
