﻿create table payment.td_status
(
   id           smallint
  ,code         varchar(64) NOT NULL
  ,name         text        NOT NULL
  ,is_finish    boolean default false not null
  ,constraint pk_status primary key(id) using index tablespace ts_tr_idx
  ,constraint uk_status unique(code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.td_status  OWNER TO acc_admin;

COMMENT ON TABLE payment.td_status IS 'Статусы обработки транзакций';
comment on column payment.td_status.id  IS 'Первичнй ключ';
comment on column payment.td_status.code  IS 'Код статуса';
comment on column payment.td_status.name is 'Наименование';
comment on column payment.td_status.is_finish is 'Признак того что это финальный статус';


CREATE TRIGGER tr_status_code
  BEFORE INSERT OR UPDATE OF code
  ON payment.td_status FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();

insert into payment.td_status(id,code,name,is_finish)
values(1,'pay_new','Новый',false)
     ,(2,'pay_reserving','Резервируется',false)
     ,(3,'pay_reserved','Зарезирвирован',false)
     ,(4,'pay_accepting','выполняется',false)
     ,(5,'pay_accepted','выполнен',true)
     ,(6,'pay_abandoning','Отменяется',false)
     ,(7,'pay_abandoned','Отменен',true)
     ,(8,'pay_denied','Отклонен',true)
     ,(11,'work_new','Новый',false)
     ,(12,'work_accepting','выполняется',false)
     ,(13,'work_reserving','Резервируется',false)
     ,(14,'work_reserved','Резервируется',false)
     ,(15,'work_commiting','Подтверждается',false)
     ,(16,'work_accepted','Выполнен',true)
     ,(17,'work_abandoning','Отменяется',false)
     ,(18,'work_abandoned','Отменен',true)
     ,(19,'work_denied','Отклонен',true)
     ,(20,'work_changed','Переключен на другой канал',true)
     ;

