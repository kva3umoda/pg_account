create table  payment.t_driver_operation
(
  driver_id 			integer
 ,op_type_id      integer
 ,func_name_get	  text
 ,func_name_save  text
 ,class_name			text
 ,is_manual       boolean default false not null
 ,constraint pk_driver_operation primary key(driver_id,op_type_id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.t_driver_operation  OWNER TO acc_admin;

comment on table payment.t_driver_operation is 'Типы оперция поддерживаемый драйвером';
comment on column payment.t_driver_operation.driver_id is 'Драйвер';
comment on column payment.t_driver_operation.op_type_id 		is 'Тип операции';
comment on column payment.t_driver_operation.func_name_get   is 'Функция БД инициализации операции';
comment on column payment.t_driver_operation.func_name_save is 'Функция БД сохранение результат';
comment on column payment.t_driver_operation.class_name is 'Типы оперция поддерживаемый драйвером';
comment on column payment.t_driver_operation.is_manual is 'Признак того что инициализация идет вручную';

alter table payment.t_driver_operation add constraint fk_driver_operation_dr foreign key(driver_id) references payment.td_driver(id);
alter table payment.t_driver_operation add constraint fk_driver_operation_op foreign key(op_type_id) references payment.td_operation_type(id);

create index ix_driver_operation_op on payment.t_driver_operation(op_type_id) tablespace ts_tr_idx;
