﻿--drop table payment.td_driver;
CREATE TABLE payment.t_con_param
(
  	 con_id			integer
    ,param_id 	 integer
  	,value		   text
    ,constraint pk_con_param primary key(con_id,param_id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

alter table  payment.t_con_param OWNER TO acc_admin;

comment on table  payment.t_con_param is 'Параметры соединения';
comment on column payment.t_con_param.con_id is 'Идентификатор соединения';
comment on column payment.t_con_param.param_id is 'Параметр драйвера';
comment on column payment.t_con_param.value is 'Знaчение';

alter table payment.t_con_param add constraint fk_con_param_con foreign key(con_id) references payment.td_connection(id);
alter table payment.t_con_param add constraint fk_con_param_prm foreign key(param_id) references global.td_param(id);

create index ix_con_param_prm on payment.t_con_param(param_id) tablespace ts_tr_idx;
