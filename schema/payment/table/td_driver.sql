﻿--drop table payment.td_driver;
CREATE TABLE payment.td_driver
(
  	 id 	    serial
  	,code       varchar
    ,name       varchar
    ,class_name varchar
    ,is_async   boolean default true not null
    ,constraint pk_driver       primary key(id) using index tablespace ts_tr_idx
    ,constraint uk_driver       unique(code) using index tablespace ts_tr_idx
    ,constraint uk_driver_queue unique(queue_code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE  payment.td_driver OWNER TO acc_admin;

comment on table  payment.td_driver is 'Таблица драйверов';
comment on column payment.td_driver.id is 'Идентификатор драйвера';
comment on column payment.td_driver.code is 'код драйвера';
comment on column payment.td_driver.name is 'Наименование драйвера';
comment on column payment.td_driver.class_name is 'Наименование класса приложения';
comment on column payment.td_driver.queue_code is 'Код очереди для драйвера';
comment on column payment.td_driver.is_async is 'Признак Cоедниения ансихронного';
