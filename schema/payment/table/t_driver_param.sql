﻿--drop table payment.td_driver;
CREATE TABLE payment.t_driver_param
(
  	 driver_id		integer
    ,param_id 	  integer
  	,value			 text
    ,constraint pk_td_driver_param primary key(driver_id,param_id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

alter table  payment.t_driver_param OWNER TO acc_admin;

comment on table  payment.t_driver_param is 'Параметры драйвера';
comment on column payment.t_driver_param.driver_id is 'Идентификатор драйвера';
comment on column payment.t_driver_param.param_id is 'Параметр драйвера';
comment on column payment.t_driver_param.value is 'Знaчение';


alter table payment.t_driver_param add constraint fk_driver_param_dr foreign key(driver_id) references payment.td_driver(id);
alter table payment.t_driver_param add constraint fk_driver_param_prm foreign key(param_id) references global.td_param(id);

create index ix_driver_param_prm on payment.t_driver_param(param_id) tablespace ts_tr_idx;