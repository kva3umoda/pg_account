﻿create table payment.td_driver_status
(
    id          serial
   ,driver_id   integer not null
   ,code        varchar not null
   ,description varchar
   ,remote_code varchar not null
   ,is_finish   boolean default false not null
   ,constraint pk_driver_status primary key(id) using index tablespace ts_tr_idx
   ,constraint uk_driver_status_code unique(driver_id,code) using index tablespace ts_tr_idx
   ,constraint pk_driver_status_remote unique(driver_id,remote_code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.td_driver_status  OWNER TO acc_admin;

comment on table payment.td_driver_status               is 'Дополнительные статусы протокола';
comment on column payment.td_driver_status.id           is 'Идентификатор';
comment on column payment.td_driver_status.driver_id    is 'Драйвер';
comment on column payment.td_driver_status.code         is 'Код ошибки';
comment on column payment.td_driver_status.description  is 'Описание ошибки';
comment on column payment.td_driver_status.remote_code  is 'Код ошибки на стороне внешней системы';
comment on column payment.td_driver_status.is_finish    is 'Признак окончательного статуса';


CREATE TRIGGER tr_driver_status_code
  BEFORE INSERT OR UPDATE OF code
  ON payment.td_driver_status FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();