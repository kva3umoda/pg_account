﻿--drop table payment.t_con_pay_ch;
CREATE TABLE payment.t_con_pay_ch
(
  	 con_id			integer
    ,pay_ch_id  integer
    ,constraint pk_con_pay_ch primary key(pay_ch_id,con_id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

alter table  payment.t_con_pay_ch OWNER TO acc_admin;

comment on table  payment.t_con_pay_ch is 'Связь канала плтежа с соединением';
comment on column payment.t_con_pay_ch.con_id is 'Идентификатор соединения';
comment on column payment.t_con_pay_ch.pay_ch_id is 'Канал платежа';


alter table payment.t_con_pay_ch add constraint fk_con_pay_ch_con foreign key(con_id) references payment.td_connection(id);
alter table payment.t_con_pay_ch add constraint fk_con_pay_ch_pay foreign key(pay_ch_id) references payment.td_pay_channel(id);

create index ix_con_pay_ch_con on payment.t_con_pay_ch(con_id) tablespace ts_tr_idx;
