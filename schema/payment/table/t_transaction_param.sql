﻿drop table payment.t_transaction_param;
create table payment.t_transaction_param
(
   tr_id        bigint  not null
  ,tr_out_id    bigint
  ,param_id     integer not null
  ,value        varchar
--  ,constraint uk_transaction_prm unique(tr_id,param_id,tr_out_id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.t_transaction_param  OWNER TO acc_admin;

comment on table payment.t_transaction_param                is 'Параметры транзакции';
comment on column payment.t_transaction_param.tr_id         is 'Идентификатор общей транзакции';
comment on column payment.t_transaction_param.tr_out_id     is 'Идентификатор общей транзакции';
comment on column payment.t_transaction_param.param_id      is 'Параметр';
comment on column payment.t_transaction_param.value         is 'Значение';


create unique index ux_tr_param_tr on payment.t_transaction_param(tr_id,param_id) tablespace ts_tr_idx where tr_out_id is null;
create unique index ux_tr_param_tr_out on payment.t_transaction_param(tr_out_id,tr_id,param_id) tablespace ts_tr_idx where tr_out_id is not null;

create index ix_tr_prm_val on payment.t_transaction_param(param_id,value) tablespace ts_tr_idx;

alter table payment.t_transaction_param add constraint fk_tr_prm_tr foreign key (tr_id) references payment.t_transaction(id);
alter table payment.t_transaction_param add constraint fk_tr_prm_tr_out foreign key (tr_out_id) references payment.t_transaction_out(id);
alter table payment.t_transaction_param add constraint fk_tr_prm_prm foreign key (param_id) references global.td_param(id);

