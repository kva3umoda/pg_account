﻿create sequence payment.seq_transaction   increment 1 minvalue 1  maxvalue 9223372036854775807 start 1  cache 100;

create table payment.t_transaction
(
   id             bigint     not null DEFAULT nextval('payment.seq_transaction')
  ,acc_id         uuid       not null
  ,amount         integer    not null
  ,delay_id       smallint   not null
  ,acc_status_id  smallint   not null
  ,out_status_id  smallint   not null
  ,created_dt     timestamp  default current_timestamp not null
  ,finish_dt      timestamp
  ,direction      smallint   not null
  ,pay_ch_id      smallint   not null
  ,constraint pk_transaction primary key(id) using index tablespace ts_tr_idx
  ,constraint ch_transaction_dir check(direction::integer in (-1,1))
)
tablespace ts_tr_dat;

ALTER TABLE payment.t_transaction  OWNER TO acc_admin;
ALTER TABLE payment.seq_transaction   OWNER TO acc_admin;

comment on table payment.t_transaction is 'Транзакции платежа';
comment on column payment.t_transaction.id  IS 'идентификатор транзакции';
comment on column payment.t_transaction.acc_id  IS 'Идентификатор счета';
comment on column payment.t_transaction.amount is 'Сумма в транзакции';
comment on column payment.t_transaction.dela_id is 'Стадийность операции';
comment on column payment.t_transaction.acc_status_id is 'Статус счета транзакции';
comment on column payment.t_transaction.out_status_id is 'Статус отработки внешенй системы';
comment on column payment.t_transaction.created_dt is 'Время создания платежа';
comment on column payment.t_transaction.finish_dt is 'Время окончания';
comment on column payment.t_transaction.direction is 'Направление движения : "-1" - исходящие транзакция, "1"-входящая транзакция, 0 - сервисная транзакция';
comment on column payment.t_transaction.pay_ch_id   is 'Канал по которому проходит перевод средств';

create index ix_transaction_acc on payment.t_transaction(acc_id,created_dt,direction) tablespace ts_tr_idx;

alter table payment.t_transaction add constraint fk_transaction_acc foreign key (acc_id) references account.t_account(id);
alter table payment.t_transaction add constraint fk_transaction_delay foreign key (delay_id) references payment.td_delay(id);
alter table payment.t_transaction add constraint fk_transaction_out_st foreign key (acc_status_id) references payment.td_status(id);
alter table payment.t_transaction add constraint fk_transaction_acc_st foreign key (out_status_id) references payment.td_status(id);


CREATE TRIGGER tr_transaction_st_plus
  AFTER INSERT OR UPDATE OF out_status_id
  ON payment.t_transaction FOR EACH ROW
  WHEN (new.direction=1)
EXECUTE PROCEDURE payment.on_transaction_st_plus();


CREATE TRIGGER tr_transaction_st_minus
  AFTER INSERT OR UPDATE OF acc_status_id,out_status_id
  ON payment.t_transaction FOR EACH ROW
  WHEN (new.direction=-1)
EXECUTE PROCEDURE payment.on_transaction_st_minus();