﻿create sequence payment.seq_operation   increment 1 minvalue 1  maxvalue 9223372036854775807 start 1  cache 100;
create table payment.t_operation
(
    id         bigint not null DEFAULT nextval('payment.seq_operation')
   ,tr_id      bigint not null
   ,tr_out_id  bigint
   ,type_id    integer  not null
   ,status_id  smallint not null
   ,event_id   integer
   ,created_dt timestamp default current_timestamp not null
   ,updated_dt    timestamp
   ,req_status_id   integer
   ,req_note        text
   ,req_dt          timestamp
   ,constraint pk_operation primary key (id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.t_operation  OWNER TO acc_admin;
ALTER TABLE payment.seq_operation   OWNER TO acc_admin;

comment on table payment.t_operation is 'Операции транзакции';
comment on column payment.t_operation.id is 'Идентификатор';
comment on column payment.t_operation.tr_id is 'Транзакция';
comment on column payment.t_operation.tr_out_id is 'Транзакция обаработки внешней системы';
comment on column payment.t_operation.type_id is 'Тип операции';
comment on column payment.t_operation.event_id is 'Сообщение в очереди';
comment on column payment.t_operation.status_id is 'Cтатус платежа';
comment on column payment.t_operation.created_dt is 'Время создания';
comment on column payment.t_operation.updated_dt is 'Время обновления состояния';
comment on column payment.t_operation.req_status_id is 'Статус ответа стороний системы';
comment on column payment.t_operation.req_note is 'Текст ответа от стороней системы';
comment on column payment.t_operation.req_dt is 'Время ответа';

create index ix_operation_tr on payment.t_operation(tr_id,tr_out_id) tablespace ts_tr_idx;

