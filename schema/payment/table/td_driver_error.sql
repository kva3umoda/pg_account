﻿create table payment.td_driver_error
(
    id          serial
   ,driver_id   integer not null
   ,code        varchar not null
   ,description varchar
   ,remote_code varchar not null
   ,is_continue    boolean default false
   ,repeat_interval integer default 0
   ,constraint pk_driver_error primary key(id) using index tablespace ts_tr_idx
   ,constraint uk_driver_error_code unique(driver_id,code) using index tablespace ts_tr_idx
   ,constraint uk_driver_error_remote unique(driver_id,remote_code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.td_driver_error  OWNER TO acc_admin;

comment on table payment.td_driver_error                is 'Ошибки протокола драйвера';
comment on column payment.td_driver_error.id            is 'Идентификатор';
comment on column payment.td_driver_error.driver_id     is 'Драйвер';
comment on column payment.td_driver_error.code          is 'Код ошибки';
comment on column payment.td_driver_error.description   is 'Описание ошибки';
comment on column payment.td_driver_error.remote_code   is 'Код ошибки на стороне внешней системы';
comment on column payment.td_driver_error.is_continue   is 'Продолжать обработку';
comment on column payment.td_driver_error.repeat_interval   is 'Интервал запроса в секундах';

CREATE TRIGGER tr_driver_error_code
  BEFORE INSERT OR UPDATE OF code
  ON payment.td_driver_error FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();
