﻿create table payment.td_delay
(
   id    smallint not null
  ,code  varchar  not null
  ,name  varchar  not null
  ,constraint pk_delay primary key(id) using index tablespace ts_tr_idx
  ,constraint uk_delay_code unique(code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

alter table payment.td_delay  OWNER TO acc_admin;

comment on table payment.td_delay is 'Cправочник стадийности платежа';
comment on column payment.td_delay.id is 'идентификатор';
comment on column payment.td_delay.code is 'код стадии';
comment on column payment.td_delay.name is 'имя стадии';


-- Триггер
create trigger tr_delay_code
  BEFORE INSERT OR UPDATE OF code
  ON payment.td_delay FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();


insert into payment.td_delay(id,code,name)
    values(1,'one','Одностадийная операция')
         ,(2,'two','Двухстадийная операция')
         ;         