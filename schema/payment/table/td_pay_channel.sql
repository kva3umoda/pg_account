﻿CREATE TABLE payment.td_pay_channel
(
    id SERIAL,
   code TEXT,
   name TEXT,
   CONSTRAINT pk_pay_channel PRIMARY KEY(id)  USING INDEX TABLESPACE ts_tr_idx
)
WITH (oids = false)
TABLESPACE ts_tr_dat;

alter table  payment.td_pay_channel OWNER TO acc_admin;

COMMENT ON TABLE payment.td_pay_channel IS 'Канал платежа';
COMMENT ON COLUMN payment.td_pay_channel.id IS 'Идентификатор';
COMMENT ON COLUMN payment.td_pay_channel.code IS 'код платежа';
COMMENT ON COLUMN payment.td_pay_channel.name IS 'Наименование';

insert into payment.td_pay_channel(code,name)
values('account','Внутрення система бонусов')
,('visa','Карта VISA')
,('mastercard','Карта MasterCard')
,('paypal','PayPal')
,('webmoney','WebMoney')
,('yandexmoney','Яндекс деньги')
,('webapp','Приложения WEB')
,('qiwi','Qiwi')
,('paypal','PayPal')
;
