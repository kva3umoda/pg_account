﻿create table payment.t_transaction_out
(
   id           bigint  not null default nextval('payment.seq_transaction')
  ,tr_id        bigint  not null
  ,tr_des_id    varchar
  ,con_id       integer    not null
  ,delay_id     smallint   not null
  ,status_id    smallint   not null
  ,astatus_id   integer
  ,cur_id       varchar(3) not null
  ,amount       integer    not null
  ,created_dt   timestamp  default localtimestamp not null
  ,finish_dt    timestamp
  ,tr_out_next_id bigint
  ,constraint pk_transaction_out primary key(id) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE payment.t_transaction_out  OWNER TO acc_admin;

comment on table payment.t_transaction_out                  is 'Транзакции отработки c внешней системой';
comment on column payment.t_transaction_out.id              is 'Идентификатор';
comment on column payment.t_transaction_out.tr_id           is 'Идентификатор общей транзакции';
comment on column payment.t_transaction_out.tr_des_id       is 'Внешний идентификатор транзакции';
comment on column payment.t_transaction_out.con_id          is 'Используемое соединение';
comment on column payment.t_transaction_out.status_id       is 'Статус транзакции';
comment on column payment.t_transaction_out.astatus_id      is 'Статус транзакции внешенй системы';
comment on column payment.t_transaction_out.amount          is 'Сумма';
comment on column payment.t_transaction_out.cur_id          is 'Валюта';
comment on column payment.t_transaction_out.created_dt      is 'Время создания';
comment on column payment.t_transaction_out.finish_dt       is 'Время окончания';
comment on column payment.t_transaction_out.tr_out_next_id  is 'Следуящая транзакция';

create index ix_transaction_out_tr on payment.t_transaction_out(tr_id) tablespace ts_tr_idx;
create unique index ux_transaction_out_des_di on payment.t_transaction_out(con_id,tr_des_id) tablespace ts_tr_idx;

alter table payment.t_transaction_out add constraint fk_transaction_out_tr foreign key (tr_id) references payment.t_transaction(id);
alter table payment.t_transaction_out add constraint fk_transaction_out_delay foreign key (delay_id) references payment.td_delay(id);
alter table payment.t_transaction_out add constraint fk_transaction_out_st foreign key (status_id) references payment.td_status(id);
alter table payment.t_transaction_out add constraint fk_transaction_out_ast foreign key (astatus_id) references payment.td_driver_status(id);
alter table payment.t_transaction_out add constraint fk_transaction_out_cur foreign key (cur_id) references global.td_currency(id);


CREATE TRIGGER tr_transaction_out_st
  AFTER INSERT OR UPDATE OF status_id
  ON payment.t_transaction_out FOR EACH ROW
  EXECUTE PROCEDURE payment.on_transaction_out_st();
  