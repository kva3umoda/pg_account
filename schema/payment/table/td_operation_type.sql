﻿create table payment.td_operation_type
(
    id 		serial
   ,code    varchar
   ,name    varchar
   ,constraint pk_operation_type primary key(id) using index tablespace ts_tr_idx
   ,constraint uk_operation_type unique(code) using index tablespace ts_tr_idx
)
tablespace ts_tr_dat;

ALTER TABLE  payment.td_operation_type OWNER TO acc_admin;

comment on table payment.td_operation_type is 'Типы операций';
comment on column payment.td_operation_type.id 	is 'Идентификатор';
comment on column payment.td_operation_type.code is 'Код ';
comment on column payment.td_operation_type.name is 'Наименование';

-- Триггер
create trigger tr_operation_type_code
  BEFORE INSERT OR UPDATE OF code
  ON payment.td_operation_type FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();

insert into payment.td_operation_type(code,name)
	values('create_payment','Пополение счета')
			 ,('confirm_payment','Подтверждения пополнения')
    	 ,('abandon_payment','Отмена зачисления')
       ,('create_withdraw','Списание средств')
       ,('confirm_withdraw','Запрос на подверждения списание')
       ,('abandon_withdraw','Списание средств')
       ,('get_payment_status','Получение статус платежа')
       ,('get_payments_info','Получение информации о транзакция');
            