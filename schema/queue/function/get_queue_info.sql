﻿CREATE OR REPLACE FUNCTION queue.get_queue_info (
  pi_queue_code text
)
RETURNS queue.t_queue AS
$body$
SELECT t.*
FROM queue.t_queue t
where t.code=lower(pi_queue_code);
$body$
LANGUAGE 'sql'
VOLATILE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 100;