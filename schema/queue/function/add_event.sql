﻿CREATE OR REPLACE FUNCTION queue.add_event (
   pi_queue_code text
  ,pi_event_type text
  ,pi_event_data text
  ,pi_start_after_sec integer = 0
  ,pi_event_extra1 text default null
  ,pi_event_extra2 text default null
  ,pi_event_extra3 text default null
)
RETURNS smallint AS
$body$
DECLARE
	l_sql text;
    l_queue_row queue.t_queue;
    l_create_dt timestamp;
    l_start_dt  timestamp;
    l_result smallint;
BEGIN
	l_queue_row:=queue.get_queue_info(pi_queue_code);
   	if l_queue_row is null then
    	l_result:=queue.create_queue(pi_queue_code);
        raise notice 'create_queue result: %s',l_result;
      	l_queue_row:=queue.get_queue_info(pi_queue_code);
--        commit;
    end if;
    if l_queue_row.is_disable_add = True then
    	return global.get_status('err_app');
    end if;

    l_sql:='insert into '||l_queue_row.event_table
      ||' ( queue_id,status_id,status_dt,start_dt,create_dt,repeat_cnt,event_type,event_data,event_extra1,event_extra2,event_extra3)'
    		||' values( %s, %s, %s::timestamp, %s::timestamp, %s::timestamp, %s, %s, %s, %s, %s, %s);';

    l_create_dt:=current_timestamp;
    l_start_dt:=l_create_dt + (pi_start_after_sec||' second')::interval;
    execute format(l_sql
    	  ,l_queue_row.id
        ,1
    	  ,quote_literal(l_create_dt)
        ,quote_literal(l_start_dt)
        ,quote_literal(l_create_dt)
        ,0
        ,quote_literal(pi_event_type)
        ,COALESCE(quote_literal(pi_event_data),'NULL')
        ,COALESCE(quote_literal(pi_event_extra1),'NULL')
        ,COALESCE(quote_literal(pi_event_extra2),'NULL')
        ,COALESCE(quote_literal(pi_event_extra3),'NULL')
        );
    return global.get_status('ok');
--EXCEPTION
--WHEN others THEN
--	raise notice '%,%',SQLERRM, SQLSTATE;
--	return global.get_status('err_app');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 2;


ALTER FUNCTION queue.add_event( pi_queue_code text
                                ,pi_event_type text
                                ,pi_event_data text
                                ,pi_start_after_sec integer
                                ,pi_event_extra1 text
                                ,pi_event_extra2 text
                                ,pi_event_extra3 text)   OWNER TO acc_admin;