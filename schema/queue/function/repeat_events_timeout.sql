﻿CREATE OR REPLACE FUNCTION queue.repeat_events_timeout
(
  pi_queue_code text
)
RETURNS integer AS
$body$
DECLARE
	-- метод вызвается для повтора сообщений
	l_queue_row queue.t_queue;
  	l_out_time 	  timestamp;
    l_req_time 	  timestamp;
    l_sql	text;
BEGIN
	l_queue_row:=queue.get_queue_info(pi_queue_code);
	if l_queue_row is null then
    	raise notice 'Очереди с именем % не сушествует',pi_queue_code;
        return global.get_status('err_app');
    end if;

    -- переводим на повтор
    l_req_time:=current_timestamp;
    l_out_time:=l_req_time-('5 minutes')::interval;
	l_sql:=format('with dat as (update %s as ev set status_id=1,status_dt=%s::timestamp,start_dt=%s::timestamp,repeat_cnt=repeat_cnt+1 '
    							||' where status_id=2 and status_dt<=%s::timestamp and  pg_advisory_unlock(ev.tableoid::integer,ev.id::integer)'
                                ||' returning ev.id::integer as id ,ev.tableoid::integer as table_oid)'
                                ||' select pg_advisory_unlock(table_oid,id) from dat'
   							   ,l_queue_row.event_table
                               ,quote_literal(l_req_time)
                               ,quote_literal(l_req_time)
                               ,quote_literal(l_out_time)
                             );
	--raise notice 'SQL %', l_sql;
   	execute l_sql;

	return global.get_status('ok');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;


ALTER FUNCTION queue.repeat_events_timeout(pi_queue_code text) OWNER TO acc_admin;