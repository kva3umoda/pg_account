﻿CREATE OR REPLACE FUNCTION queue.repeat_event (
  pi_queue_code text,
  pi_event_id integer,
  pi_start_after_sec integer = 0
)
RETURNS integer AS
$body$
DECLARE
	-- метод вызвается для повтора сообщений
	l_queue_row queue.t_queue;
  	l_req_time 	timestamp;
    l_start_dt 	  timestamp;
BEGIN
	l_queue_row:=queue.get_queue_info(pi_queue_code);
	if l_queue_row is null then
    	raise notice 'Очереди с именем % не сушествует',pi_queue_code;
        return global.get_status('err_app');
    end if;
    l_req_time:=current_timestamp;
    l_start_dt:=l_req_time + (pi_start_after_sec||' second')::interval;
   	execute format('update %s set status_id=1,status_dt=%s::timestamp,start_dt=%s::timestamp,repeat_cnt=repeat_cnt+1 where id=%s and status_id=2;'
   							   ,l_queue_row.event_table
                               ,quote_literal(l_req_time)
                               ,quote_literal(l_start_dt)
                               ,pi_event_id
                             );
   	execute format('select pg_advisory_unlock(ev.tableoid::integer,ev.id::integer) from %s as ev where id=%s;'
   							   ,l_queue_row.event_table
                               ,pi_event_id
                             );
	return get_status('ok');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;