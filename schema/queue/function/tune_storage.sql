﻿CREATE OR REPLACE FUNCTION queue.tune_storage(pi_queue_code text)
  RETURNS integer AS
$BODY$
declare
  l_queue_row queue.t_queue;
  l_sql       text;
begin
  select *
  into l_queue_row
  from queue.t_queue q
  where q.code=lower(pi_queue_code);

  if not found then
      return -1;
  end if;

  l_sql:='alter table '||l_queue_row.event_table||' set (fillfactor = 95,autovacuum_enabled=off, toast.autovacuum_enabled =off)';
  execute l_sql;
  return 0;
end;
$BODY$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;


ALTER FUNCTION queue.tune_storage(pi_queue_code text) OWNER TO acc_admin;
