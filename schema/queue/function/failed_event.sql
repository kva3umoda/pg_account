﻿CREATE OR REPLACE FUNCTION queue.failed_event(
  pi_queue_code text,
  pi_event_id integer,
  pi_failed_note text default null
)
RETURNS integer AS
$body$
DECLARE
	-- метод вызвается для повтора сообщений
	l_queue_row queue.t_queue;
  	l_req_time 	timestamp;
BEGIN
	l_queue_row:=queue.get_queue_info(pi_queue_code);
	if l_queue_row is null then
    	raise notice 'Очереди с именем % не сушествует',pi_queue_code;
        return global.get_status('err_app');
    end if;
    l_req_time:=current_timestamp;
   	execute format('update %s set status_id=4,status_dt=%s::timestamp,failed_note=%s where id=%s;'
   							   ,l_queue_row.event_table
                               ,quote_literal(l_req_time)
                               ,quote_literal(pi_failed_note)
                               ,pi_event_id
                             );
   	execute format('select pg_advisory_unlock(ev.tableoid::integer,ev.id::integer) from %s as ev where id=%s;'
   							   ,l_queue_row.event_table
                               ,pi_event_id
                             );
	return get_status('ok');
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;