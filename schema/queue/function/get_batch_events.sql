﻿CREATE OR REPLACE FUNCTION queue.get_batch_events (
  pi_queue_code text
)
RETURNS TABLE (
  id integer,
  queue_id integer,
  queue_code text,
  ev_type text,
  ev_data text,
  event_extra1 text,
  event_extra2 text,
  event_extra3 text
) AS
$body$
DECLARE
  l_sql_select	text;
  l_sql_update	text;
  l_queue_row queue.t_queue;
  l_req_time timestamp;
BEGIN
	l_sql_select:='select ev.id,ev.queue_id,q.code as queue_code,ev.event_type,ev.event_data,ev.event_extra1,ev.event_extra2,ev.event_extra3'
            ||' from %s as ev '
            ||' left join queue.t_queue q on q.id=ev.queue_id'
            ||' where status_id=1 '
            ||'      and start_dt<=%s::timestamp'
            ||'      and pg_try_advisory_lock(ev.tableoid::integer,id::integer)'
            ||' order by start_dt'
            ||' limit %s';
	l_sql_update:='update %s set status_id=%s,status_dt=%s where id=%s' ;
	l_queue_row:=queue.get_queue_info(pi_queue_code);
    if l_queue_row is null then
    	raise notice 'Очередь не существует %',pi_queue_code;
    	return;
    elsif l_queue_row.is_paused then
      	raise notice 'Очередь на паузе %',pi_queue_code;
    	return;
    end if;
	l_req_time:= current_timestamp;
    for id,queue_id,queue_code,ev_type, ev_data,event_extra1,event_extra2,event_extra3
        in execute format(l_sql_select,l_queue_row.event_table,quote_literal(l_req_time),l_queue_row.batch_size)
    loop
    	execute format(l_sql_update,l_queue_row.event_table,2,quote_literal(l_req_time),id);
        return next;
    end loop;
    --select pg_advisory_unlock_all();
    return;
--EXCEPTION
--WHEN exception_name THEN
--  statements;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
RETURNS NULL ON NULL INPUT
SECURITY INVOKER
COST 10 ROWS 100;