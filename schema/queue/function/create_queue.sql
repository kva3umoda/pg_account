﻿-- Function: pgq.create_queue(text)

-- DROP FUNCTION pgq.create_queue(text);
CREATE OR REPLACE FUNCTION queue.create_queue(pi_queue_code text)
  RETURNS integer AS
$BODY$
-- ----------------------------------------------------------------------
-- Создает новую очередь
-- ----------------------------------------------------------------------
declare
    l_tbl_pfx text;
    l_tbl_name text;
    l_seq_name text;
    l_sql		     text;
    l_id 			 integer;
begin
    if pi_queue_code is null then
        raise exception 'Invalid NULL value';
    end if;

    -- check if exists
    perform 1 from queue.t_queue where code = lower(pi_queue_code);
    if found then
        return global.get_status('err_app');
    end if;

    -- insert event
    l_id := nextval('queue.t_queue_id_seq');
    l_tbl_name:='queue.ev_'||lower(pi_queue_code);
	l_seq_name:=l_tbl_name||'_seq';
    insert into queue.t_queue(id,code,event_table,event_seq,is_paused,is_disable_add,batch_size)
    	values(l_id,lower(pi_queue_code),l_tbl_name,l_seq_name,false,false,1);

    -- create seqs
	RAISE NOTICE 'CREATE SEQUENCE % INCREMENT 1 START 1 CACHE 100;',l_seq_name;
    execute 'CREATE SEQUENCE ' || l_seq_name||' INCREMENT 1 START 1 CACHE 100;';

    -- create data tables
	RAISE NOTICE 'CREATE TABLE % (check ()) INHERITS (queue.ev_template)', l_tbl_name;
    execute 'CREATE TABLE ' || l_tbl_name || ' (check(queue_id='||l_id||')) '
            || ' INHERITS (queue.ev_template) tablespace ts_q_dat';

	--RAISE NOTICE 'ALTER TABLE ' || l_tbl_name || ' ALTER COLUMN id '
    --            || ' SET DEFAULT nextval(' ||l_seq_name || ');';
    execute 'ALTER TABLE ' || l_tbl_name || ' ALTER COLUMN id '
                || ' SET DEFAULT nextval(''' ||l_seq_name || ''');';

	--RAISE NOTICE 'CREATE INDEX ' ||'ix_ev_'||pi_queue_code||'_id on '
    --            || l_tbl_name || ' (id);';
    execute 'CREATE INDEX ' ||'ix_ev_'||pi_queue_code||'_id on '
                || l_tbl_name || ' (id) tablespace ts_q_idx;';

    execute 'CREATE INDEX ' ||'ix_ev_'||pi_queue_code||'_start on '
                || l_tbl_name || ' (status_id,start_dt) tablespace ts_q_idx WHERE status_id=1;';

    execute 'CREATE INDEX ' ||'ix_ev_'||pi_queue_code||'_proc on '
                || l_tbl_name || ' (status_id,status_dt) tablespace ts_q_idx WHERE status_id=2;';
    return global.get_status('ok');
--exception
--	when others then
--    	return 1;
end;
$BODY$
LANGUAGE plpgsql VOLATILE SECURITY DEFINER
COST 100;

ALTER FUNCTION queue.create_queue(text)   OWNER TO acc_admin;
