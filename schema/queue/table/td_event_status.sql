﻿create table queue.td_event_status
(
	id smallint
    ,code text
    ,name text
    ,constraint pk_event_status primary key(id) using index tablespace ts_q_idx
)
tablespace ts_q_dat;

comment on table queue.td_event_status is 'Статусы сообщений';
comment on column queue.td_event_status.id    is 'Первичнй ключ';
comment on column queue.td_event_status.code  is 'Код статуса';
comment on column queue.td_event_status.name  is 'Наименование';

insert into queue.td_event_status(id,code,name)
	values(1,'new','новый'),(2,'process','В процессе'),(3,'finish','Обработан'),(4,'fault','Ошибка');
