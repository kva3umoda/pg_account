﻿drop table queue.t_queue;
CREATE TABLE queue.t_queue
(
   id SERIAL
  ,code text not null
  ,event_table text not null
  ,event_seq 	 text not null
  ,is_paused  boolean  default false not null
  ,is_disable_add boolean default false not null
  ,batch_size 	smallint default 1 not null
  ,constraint pk_queue primary key(id) using index tablespace ts_q_idx
  ,constraint uk_queue unique(code) using index tablespace ts_q_idx
)
tablespace ts_q_dat;

ALTER TABLE queue.t_queue OWNER TO acc_admin;

comment on table queue.t_queue is 'Таблица очередей';
comment on column queue.t_queue.id is 'Идентификатор очереди';
comment on column queue.t_queue.code is 'Уникальный код очереди';
comment on column queue.t_queue.event_table is 'Таблица с событиями';
comment on column queue.t_queue.event_seq is 'сиквина событий';
comment on column queue.t_queue.is_paused is 'Признак паузы очереди';
comment on column queue.t_queue.is_disable_add is 'Свойство отключено вставка значений';
comment on column queue.t_queue.batch_size is 'Размер пачки из очереди';
