﻿create table queue.ev_template
(
   id 		bigint 	not null
  ,queue_id	integer not null
  ,status_id	integer not null
  ,status_dt	timestamp not null
  ,start_dt 	timestamp not null
  ,create_dt 	timestamp not null
  ,repeat_cnt integer default 0 not null
  ,event_type	text
  ,event_data text
  ,event_extra1 text
  ,event_extra2 text
  ,event_extra3 text
  ,failed_note  text
)
tablespace ts_q_dat;

ALTER TABLE queue.ev_template OWNER TO acc_admin;

comment on table queue.ev_template is 'Таблица шаблон для создание таблиц пулов очередей.';
comment on column queue.ev_template.id is 'Идентификатор сообщения ';
comment on column queue.ev_template.queue_id is 'id очереди';
comment on column queue.ev_template.status_id is 'статус сообщения';
comment on column queue.ev_template.status_dt is 'время перехода на данный статус';
comment on column queue.ev_template.start_dt is 'Время старта';
comment on column queue.ev_template.create_dt is 'Время cоздания';
comment on column queue.ev_template.repeat_cnt is 'Повтор сообщения';
comment on column queue.ev_template.event_type is 'тип сообщения';
comment on column queue.ev_template.event_data is 'данные сообщения';
comment on column queue.ev_template.event_extra1 is 'Расширенные данные 1';
comment on column queue.ev_template.event_extra2 is 'Расширенные данные 2';
comment on column queue.ev_template.event_extra3 is 'Расширенные данные 3';
comment on column queue.ev_template.failed_note is 'Текст с причиной выпадения в ошибку';

