﻿CREATE OR REPLACE FUNCTION global.get_param (
  in pi_code varchar
)
RETURNS INTEGER
AS
$body$
declare
  l_id  integer;
begin
  select id::integer
  into l_id
  from global.td_param
  where code=lower(pi_code);
  if NOT FOUND then
    raise exception 'Не верно указан код "%" параметра в global',lower(pi_code);
  end if;
  return l_id;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;


ALTER FUNCTION global.get_param(character varying)  OWNER TO acc_admin;
GRANT EXECUTE ON FUNCTION global.get_param(character varying) TO public;
