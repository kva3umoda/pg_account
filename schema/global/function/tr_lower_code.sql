﻿CREATE OR REPLACE FUNCTION global.tr_lower_code (
)
RETURNS trigger AS
$body$
DECLARE

BEGIN
   new.code:=lower(NEW.code);
   return new;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

ALTER FUNCTION global.get_status(character varying)  OWNER TO acc_admin;
GRANT EXECUTE ON FUNCTION global.get_status(character varying) TO public;
