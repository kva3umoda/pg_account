﻿CREATE OR REPLACE FUNCTION global.get_status (
  in pi_code varchar
)
RETURNS integer
AS
$body$
  select id::integer
  from global.td_status
  where code=lower(pi_code)
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION global.get_status(character varying)  OWNER TO acc_admin;
GRANT EXECUTE ON FUNCTION global.get_status(character varying) TO public;
