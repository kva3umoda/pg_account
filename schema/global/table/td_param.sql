﻿CREATE TABLE global.td_param
(
  id       SMALLSERIAL
  ,code     VARCHAR NOT NULL
  ,name   VARCHAR NOT NULL
  ,data_type char(1)
  ,constraint pk_param primary key(id) using index tablespace ts_global_idx
  ,constraint uk_param unique(code) using index tablespace ts_global_dat
) TABLESPACE ts_global_dat;

ALTER TABLE global.td_param   OWNER TO acc_admin;

COMMENT ON TABLE global.td_param IS 'Глобальные парамертры';
COMMENT ON COLUMN global.td_param.id  IS 'Первичнй ключ';
COMMENT ON COLUMN global.td_param.code  IS 'Код параметра';
comment on column global.td_param.name is 'Наименование параметра';
COMMENT ON COLUMN global.td_param.data_type  IS 'тип данных, S-Строка, N-Число, D-дата, T-временный штамп, J- json объект';

alter table global.td_param add constraint ch_param check(data_type in ('S','N','D','T','J'));


-- Триггер
create trigger tr_param_code
  BEFORE INSERT OR UPDATE OF code
  ON global.td_param FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();

insert into global.td_param(code,name,data_type)
    values('URL','Ссылка','S')

insert into global.td_param(code,name,data_type)
    values('agent_id','Идентификатор агента','S');

insert into global.td_param(code,name,data_type)
    values('sign','Подпись','S');

insert into global.td_param(code,name,data_type)
    values('to_account','Идентификатор счета','S');

insert into global.td_param(code,name,data_type)
    values('action','Действие','S');

insert into global.td_param(code,name,data_type)
    values('comment','Комментарий','S');

insert into global.td_param(code,name,data_type)
    values('abandon_comment','Комментарий отмены','S');

insert into global.td_param(code,name,data_type)
    values('cur_id','Валюта','S');