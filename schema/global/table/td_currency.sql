﻿CREATE TABLE global.td_currency
(
   id   varchar(3)
  ,name varchar NOT NULL
  ,constraint pk_currency primary key(id) using index tablespace ts_global_idx
)
TABLESPACE ts_global_dat;

ALTER TABLE global.td_currency  OWNER TO acc_admin;

COMMENT ON TABLE global.td_currency IS 'Валюты в системе';
COMMENT ON COLUMN global.td_currency.id  IS 'Первичнй ключ ';
comment on column global.td_currency.name is 'Наименование';

insert into global.td_currency(id,name)
values('BAL','Баллы')
    ,('RUR','Рубли')
    ,('USD','Доллары')
    ,('EUR','Евро');
