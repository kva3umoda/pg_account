﻿CREATE TABLE global.td_status
(
   id   smallint
  ,code varchar NOT NULL
  ,name varchar NOT NULL
  ,constraint pk_status primary key(id) using index tablespace ts_global_idx
  ,constraint uk_status unique(code) using index tablespace ts_global_dat
)
TABLESPACE ts_global_dat;

ALTER TABLE global.td_status
  OWNER TO acc_admin;

COMMENT ON TABLE global.td_status IS 'Глобальные статусы';
COMMENT ON COLUMN global.td_status.id  IS 'Первичнй ключ';
COMMENT ON COLUMN global.td_status.code  IS 'Код статуса';
comment on column global.td_status.name is 'Наименование';

-- Триггер
create trigger tr_status_code
  BEFORE INSERT OR UPDATE OF code
  ON global.td_status FOR EACH ROW
  EXECUTE PROCEDURE global.tr_lower_code();

insert into global.td_status(id,code,name)
values(-1,'err_app','Прикалдная ошибка')
    ,(-2,'err_sys','Системная ошибка')
    ,(-3,'err_no_found','Все ок, но ничего делать не нужно')
    ,(-4,'err_dub_value','Дублирующей значение')
    ,(-5,'err_many_rows','Можество строк')
    ,(-6,'err_param_check','Ошибка в параметрах')
    ,(-7,'err_acc_denied','Нет доступа')
    ,(0,'no','Ничего не сделано')
    ,(1,'ok','ok')
    ,(2,'not_action','Все ок, но ничего делать не нужно')
    ,(3,'repeat','Повторить запрос')
    ,(4,'repeat_wait_5_minut','Повторить запрос через  5 минут')
    ,(5,'repeat_wait_1_hour','Повторить запрос через  1 час')
    ,(6,'repeat_wait_1_day','Повторить запрос через  1 день');
