﻿CREATE OR REPLACE FUNCTION account.abandon_payment_out(
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_params    json
)
RETURNS void AS
$body$
-- Пополение счета
DECLARE
  l_op_tp_code varchar:='abandon_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_out_row payment.t_transaction_out;
  l_tr_row     payment.t_transaction;
  l_op_row      payment.t_operation;

  l_des_op_tp_code  varchar:='abandon_withdraw';
  l_des_op_tp_id    integer:=payment.get_operation_type(l_des_op_tp_code);
  l_des_acc_id      integer;
  l_des_tr_id       bigint;
  l_des_tr_out_row  payment.t_transaction_out;
  l_des_tr_row      payment.t_transaction;
  l_des_op_row      payment.t_operation;

  l_des_status_id smallint;
  l_des_status_note varchar;

--  l_acc_id uuid;
  l_abandon_comment varchar;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  l_tr_out_row:=payment.get_transaction_out_row(pi_tr_out_id);

  perform payment.save_tr_params(pi_tr_id,pi_tr_out_id,pi_params);
  l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('work_abandoning'),null::integer);

  -- Получаем идентификатор счета куда собираемся перечислять счет
  --l_acc_id:=payment.get_tr_param(pi_tr_id,pi_tr_out_id,'acc_id')::uuid;
  l_abandon_comment:=payment.get_tr_param(pi_tr_id,'abandon_comment');
  begin
    l_des_tr_out_row:=payment.get_transaction_out_row(l_tr_out_row.con_id,pi_tr_out_id::varchar);
    -- Блокировка транзацкции
    l_des_tr_row:=payment.get_transaction_row(l_des_tr_out_row.tr_id,true);

    -- Создание операции отмены
    l_des_op_row:=payment.add_operation_unique(l_des_tr_id,l_des_tr_out_row.id,l_des_op_tp_id,payment.get_status('work_abandoning'),null::integer);
    perform payment.set_operation_status(l_des_op_row.id,payment.get_status('work_abandoned'),null::integer,l_abandon_comment,localtimestamp);
    perform payment.set_transaction_out_status(l_des_tr_out_row.id,payment.get_status('work_abandoned'));
    l_des_status_id:=payment.get_status('work_abandoned');
    l_des_status_note:=null;
  exception
    when others then
      RAISE NOTICE 'ERROR : %',SQLERRM;
      l_des_status_id:=payment.get_status('work_denied');
      l_des_status_note:=format('Ошибка при зачисение на счет "%s": %s',l_acc_id,SQLERRM);
  end;

  perform payment.set_operation_status(l_op_row.id,l_des_status_id,null::integer,l_des_status_note,localtimestamp);
  perform payment.set_transaction_out_status(l_tr_out_row.id,l_des_status_id);

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION account.create_payment_out(bigint,bigint,json)  OWNER TO acc_admin;