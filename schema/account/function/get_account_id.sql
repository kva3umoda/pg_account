﻿CREATE OR REPLACE FUNCTION account.get_account_id(
  in pi_user_id uuid
)
RETURNS uuid
AS
$body$
  select id
  from account.t_account
  where user_id=pi_user_id
$body$
LANGUAGE 'sql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION account.get_account_id(uuid)  OWNER TO acc_admin;

