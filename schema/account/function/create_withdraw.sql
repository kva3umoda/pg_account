﻿CREATE OR REPLACE FUNCTION account.create_withdraw(
   pi_tr_id bigint
)
RETURNS void AS
$body$
-- Списание со счета
DECLARE
  l_op_tp_code varchar:='create_withdraw';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_row payment.t_transaction;
  l_op_row payment.t_operation;
  l_acc_row account.t_account;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  -- блокировка счета
  l_acc_row:=account.get_account_row(l_tr_row.acc_id,true);
  if l_tr_row.direction::integer != -1 then
    raise exception 'Не допустимое направление транзакции для опреаци %',l_op_tp_code;
  end if;


  if l_tr_row.out_status_id = ANY (payment.get_status('work_abandoning','work_abandoned')) then
    raise exception 'Внешняя транзакция отменяется/отменен, списание не возможно';
  elsif l_tr_row.out_status_id = (payment.get_status('work_denied'))  then
    raise exception 'Внешняя транзакция c ошибкой, списание не возможно';
  end if;

  -- для одностадийной операции
  if l_tr_row.delay_id=payment.get_delay('one') then
    if l_tr_row.out_status_id!=payment.get_status('work_accepted') then
      raise exception 'Внешная транзакция не оконченя, операция запрещена';
    end if;

    if l_tr_row.acc_status_id = ANY (payment.get_status('pay_new','pay_accepting')) then
      l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_accepting'),null::integer);
      perform account.sub_money(l_tr_row.acc_id,l_tr_row.amount);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_accepted'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_accepted'));
      raise notice 'Со счет % списана сумма %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    elsif l_tr_row.acc_status_id = (payment.get_status('pay_accepted')) then
      raise notice 'Операция уже исполенена';
      return;
    elsif l_tr_row.acc_status_id = ANY (payment.get_status('pay_abandoning','pay_abandoned')) then
      raise exception 'Транзакция отменяется';
      return;
    elseif l_tr_row.acc_status_id = (payment.get_status('pay_denied')) then
      raise exception 'Транзакция отменяется';
      return;
    end if;
  elsif l_tr_row.delay_id=payment.get_delay('two') then
    if l_tr_row.out_status_id not in (payment.get_status('work_new'),payment.get_status('work_accepting'),payment.get_status('work_accepted'),payment.get_status('work_reserving'),payment.get_status('work_reserved'))then
      raise exception 'Внешняя транзакция не в процессе обработки';
    end if;

    if l_tr_row.acc_status_id = ANY (payment.get_status('pay_new','pay_reserving')) then
      l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_reserving'),null::integer);
      perform account.reserve_money(l_tr_row.acc_id,l_tr_row.amount);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_reserved'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_reserved'));
      raise notice 'Резервирование на счете % на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    elsif l_tr_row.acc_status_id = ANY (payment.get_status('pay_accepting','pay_accepted')) then
      raise notice 'Ничего не сделано операция уже выполнена';
      return;
    elsif l_tr_row.acc_status_id = ANY (payment.get_status('pay_abandoning','pay_abandoned')) then
      raise exception 'Платеж отменяется';
      return;
    elseif l_tr_row.acc_status_id = (payment.get_status('pay_denied')) then
      raise exception 'Платеж на статусе с ошибкой';
      return;
    end if;
  end if;

  raise exception 'Не предвиденая ошибка при списание средств со счета % ',l_tr_row.acc_id;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 2;

ALTER FUNCTION account.create_withdraw(bigint)  OWNER TO acc_admin;