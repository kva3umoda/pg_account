﻿CREATE OR REPLACE FUNCTION account.reserve_money (
  pi_acc_id uuid,
  pi_amount integer
)
RETURNS integer AS
$body$
-- резервирования денег на счете
DECLARE
    l_rowcount integer;
begin
    if pi_amount is null then
        raise exception 'Сумма = NULL' using hint=global.get_status('err_param_check');
    elsif pi_amount<=0 then
        raise exception 'Сумма должна быть больше нуля (%)',pi_amoutn using hint=global.get_status('err_param_check');
    end if;

    update account.t_account as acc
    set amount=amount - pi_amount
        ,amount_reserved=amount_reserved+pi_amount
    where acc.id=pi_acc_id
            and acc.amount>=pi_amount;
    GET DIAGNOSTICS l_rowcount=ROW_COUNT;
    if l_rowcount=0 then
        raise exception 'Не достаточно средства на счете ';
    end if;
    return global.get_status('ok');
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION account.reserve_money(pi_acc_id uuid, pi_amount integer)  OWNER TO acc_admin;