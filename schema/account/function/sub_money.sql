﻿CREATE OR REPLACE FUNCTION account.sub_money (
  pi_acc_id uuid,
  pi_amount integer,
  pi_is_admin boolean default false
)
RETURNS void AS
$body$
-- снятия денег с незарезервированных средств счета
DECLARE
    l_rowcount integer;
begin
  if pi_amount is null then
      raise exception 'Сумма = NULL' using hint=global.get_status('err_param_check');
  elsif pi_amount<=0 then
      raise exception 'Сумма должна быть больше нуля (%)',pi_amoutn using hint=global.get_status('err_param_check');
  end if;

  if pi_is_admin then
    -- C админскими правами - операция без проверки допсутимого минимума
    update account.t_account as acc
    set amount=amount - pi_amount
    where acc.id=pi_acc_id;
  else
    update account.t_account as acc
    set amount=amount - pi_amount
    where acc.id=pi_acc_id
            and acc.amount>=pi_amount;
    GET DIAGNOSTICS l_rowcount=ROW_COUNT;
    if l_rowcount=0 then
      raise exception 'Не достаточно средства на счете ';
    end if;
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION account.sub_money(pi_acc_id uuid, pi_amount integer, pi_is_admin boolean)  OWNER TO acc_admin;