﻿CREATE OR REPLACE FUNCTION account.confirm_payment (
  pi_tr_id bigint
)
RETURNS void AS
$body$
-- Подтверждение пополение счета
DECLARE
  l_op_tp_code varchar:='confirm_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_row payment.t_transaction;
  l_op_row payment.t_operation;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  if l_tr_row.direction!=1 then
    raise exception 'Не допустимое направление транзакции для опреаци %',l_op_tp_code;
  end if;

  if l_tr_row.out_status_id in (payment.get_status('work_abandoning'),payment.get_status('work_abandonend')) then
    raise exception 'Внешняя транзакция отменяется/отменен, зачисление не возможно';
  elsif l_tr_row.out_status_id in (payment.get_status('work_denied'))  then
    raise exception 'Внешняя транзакция c ошибкой, зачисление не возможно';
  end if;

  -- для одностадийной операции
  if l_tr_row.delay_id=payment.get_delay('one') then
    raise exception 'Данная транзакция % является одностадийная',pi_tr_id;
  elsif l_tr_row.delay_id=payment.get_delay('two') then
    if l_tr_row.acc_status_id in (payment.get_status('pay_reserved'),payment.get_status('pay_accepting')) then
      l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_accepting'),null::integer);
      perform account.add_money(l_tr_row.acc_id,l_tr_row.amount);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_accepted'),null,pi_comment,current_timestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_accepted'));
      raise notice 'Счет % пополнен на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_accepted')) then
      raise notice 'Ничего не сделано операция уже выполнена';
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_new'),payment.get_status('pay_reserving')) then
      raise exception 'Данная транзакция двух стадийная. требутеся окончание резвирование';
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_abandoning'),payment.get_status('pay_abandoned')) then
      raise exception 'Данная транзакция отменяется';
      return;
    elseif l_tr_row.acc_status_id in (payment.get_status('pay_denied')) then
      raise exception 'Данная транзакция с ошибкой';
      return;
    end if;
  end if;

  raise exception 'Не предвиденая ошибка при подтверждение транзакции';
  return;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION account.confirm_payment(bigint)  OWNER TO acc_admin;