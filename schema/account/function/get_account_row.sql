﻿CREATE OR REPLACE FUNCTION account.get_account_row(
  pi_acc_id   account.t_account.id%type
 ,pi_is_lock  boolean
)
RETURNS account.t_account AS
$body$
declare
  l_row account.t_account;
BEGIN
  if pi_is_lock then
    select t.*
    into l_row
    from account.t_account t
    where t.id=pi_acc_id
    for update;
  else
    select t.*
    into l_row
    from account.t_account t
    where t.id=pi_acc_id;
  end if;
  return l_row;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 10;

ALTER FUNCTION account.get_account_row(account.t_account.id%type, boolean) OWNER TO acc_admin;
