﻿CREATE OR REPLACE FUNCTION account.add_money (
  pi_acc_id uuid,
  pi_amount integer
)
RETURNS void AS
$body$
DECLARE
    l_rowcount integer;
begin
  if pi_amount is null then
      raise exception 'Сумма = NULL' using hint=global.get_status('err_param_check');
  elsif pi_amount<=0 then
      raise exception 'Сумма должна быть больше нуля (%)',pi_amoutn using hint=global.get_status('err_param_check');
  end if;

  update account.t_account as acc
  set amount=amount+pi_amount
  where acc.id=pi_acc_id;
  GET DIAGNOSTICS l_rowcount=ROW_COUNT;
  if l_rowcount=0 then
      raise exception 'Не найден счет с %',pi_acc_id;
  end if;
end;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 100;

ALTER FUNCTION account.add_money(pi_acc_id uuid, pi_amount integer)  OWNER TO acc_admin;