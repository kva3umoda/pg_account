﻿CREATE OR REPLACE FUNCTION account.create_payment (
  pi_tr_id bigint
)
RETURNS void AS
$body$
-- Пополение счета
DECLARE
  l_op_tp_code varchar:='create_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_row payment.t_transaction;
  l_op_row payment.t_operation;
  l_acc_row account.t_account;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  -- блокировка счета
  l_acc_row:=account.get_account_row(l_tr_row.acc_id,true);
  if l_tr_row.direction::integer!=1 then
    raise exception 'Не допустимое направление транзакции для опреаци %',l_op_tp_code;
  end if;

  if l_tr_row.out_status_id in (payment.get_status('work_abandoning'),payment.get_status('work_abandoned')) then
    raise exception 'Внешняя транзакция отменяется/отменен, зачисление не возможно';
  elsif l_tr_row.out_status_id in (payment.get_status('work_denied'))  then
    raise exception 'Внешняя транзакция c ошибкой, зачисление не возможно';
  end if;

  -- для одностадийной операции
  if l_tr_row.delay_id=payment.get_delay('one') then
    if l_tr_row.acc_status_id in (payment.get_status('pay_new'),payment.get_status('pay_accepting')) then
      l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_accepting'),null::integer);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_accepted'),null::integer,null,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_accepted'));
      perform account.add_money(l_tr_row.acc_id,l_tr_row.amount);

      raise notice 'Счет % пополнен на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_accepted')) then
      raise notice 'Ничего не сделано операция уже выполнена';
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_abandoning'),payment.get_status('pay_abandoned')) then
      raise exception 'Платеж отменяется';
      return;
    elseif l_tr_row.acc_status_id in (payment.get_status('pay_denied')) then
      raise exception 'Платеж с ошибкой';
      return;
    end if;
  elsif l_tr_row.delay_id=payment.get_delay('two') then
    if l_tr_row.acc_status_id in (payment.get_status('pay_new'),payment.get_status('pay_reserving')) then
      l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_reserving'),null::integer);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_reserved'),null::integer,null,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_reserved'));
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_accepting'),payment.get_status('pay_accepted')) then
      raise notice 'Ничего не сделано операция уже выполнена';
      return;
    elsif l_tr_row.acc_status_id in (payment.get_status('pay_abandoning'),payment.get_status('pay_abandoned')) then
      raise exception 'Платеж отменяется';
      return;
    elseif l_tr_row.acc_status_id in (payment.get_status('pay_denied')) then
      raise exception 'Платеж с ошибкой';
      return;
    end if;
  end if;

  raise exception 'Не предвиденая ошибка при создаие платежа';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION account.create_payment(bigint)  OWNER TO acc_admin;