﻿CREATE OR REPLACE FUNCTION account.abandon_withdraw(
   pi_tr_id bigint
)
RETURNS void AS
$body$
-- Отмена списание со сче счета
/* в случае если */
DECLARE
  l_op_tp_code varchar:='abandon_withdraw';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_row payment.t_transaction;
  l_op_row payment.t_operation;
  l_acc_row account.t_account;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  -- блокировка счета
  l_acc_row:=account.get_account_row(l_tr_row.acc_id,true);
  if l_tr_row.direction::integer != -1 then
    raise exception 'Не допустимое направление транзакции для опреаци %',l_op_tp_code;
  end if;

  if l_tr_row.out_status_id != ALL (payment.get_status('work_abandoned','work_denied')) then
    raise exception 'Не допустимая операция % для транзакции у которой не произашла отмена операции';
  end if;

  if l_tr_row.acc_status_id != (payment.get_status('pay_abandoned')) then
    l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_abandoning'),null::integer);
    if l_tr_row.acc_status_id = (payment.get_status('pay_accepted')) then
      perform account.add_money(l_tr_row.acc_id,l_tr_row.amount);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_abandoned'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_abandoned'));
      return;
    elsif l_tr_row.acc_status_id = (payment.get_status('pay_reserved')) then
      perform account.cancel_money(l_tr_row.acc_id,l_tr_row.amount);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_abandoned'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_abandoned'));
      raise notice 'По счету % произошла отмена транзакции на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    else
      raise exception 'не допустимая операция для транзакции где acc_status=% ',l_tr_row.acc_status_id;
    end if;
  elsif l_tr_row.acc_status_id = (payment.get_status('pay_abandoned')) then
    raise notice 'Отмена транзакции % была уже произведена ',pi_tr_id;
    return;
  end if;

  raise exception 'Не предвиденая ошибка при отмене зачисления на счете %, (acc_status:%)',l_tr_row.acc_id,l_tr_row.acc_status_id;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 2;

ALTER FUNCTION account.abandon_withdraw(bigint)  OWNER TO acc_admin;