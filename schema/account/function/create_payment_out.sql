﻿CREATE OR REPLACE FUNCTION account.create_payment_out(
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_params    json
)
RETURNS void AS
$body$
-- Пополение счета
DECLARE
  l_op_tp_code varchar:='create_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_out_row payment.t_transaction_out;
  l_tr_row     payment.t_transaction;
  l_op_row      payment.t_operation;

  l_des_op_tp_code  varchar:='create_withdraw';
  l_des_op_tp_id    integer:=payment.get_operation_type(l_des_op_tp_code);
  l_des_tr_id       bigint;
  l_des_tr_out_row  payment.t_transaction_out;
  l_des_tr_row      payment.t_transaction;
  l_des_op_row      payment.t_operation;

  l_des_status_id smallint;
  l_des_status_note varchar;

  l_acc_id uuid;
  l_comment varchar;
  l_err_msg varchar;
  --l_value integer;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  l_tr_out_row:=payment.get_transaction_out_row(pi_tr_out_id);

  perform payment.save_tr_params(pi_tr_id,pi_tr_out_id,pi_params);
  l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('work_accepting'),null::integer);

  -- Получаем идентификатор счета куда собираемся перечислять счет
  l_acc_id:=account.get_account_id(payment.get_tr_param(pi_tr_id,pi_tr_out_id,'to_account')::uuid);
  l_comment:=payment.get_tr_param(pi_tr_id,'comment');
  begin
    --l_value:=1/0;
    l_des_tr_id:=payment.create_transaction( /*pi_acc_id=>*/l_acc_id
                                            ,/*pi_direction=>*/1
                                            ,/*pi_amount=>*/l_tr_out_row.amount
                                            ,/*pi_comment=>*/l_comment
                                            ,/*pi_pay_ch_id=>*/l_tr_row.pay_ch_id
                                            ,/*pi_out_cur_id=>*/l_tr_out_row.cur_id
                                            ,/*pi_out_amount=>*/l_tr_out_row.amount
                                            ,/*pi_des_id=>*/pi_tr_out_id::varchar
                                            ,/*pi_out_data*/null::json
                                         );
    l_des_tr_out_row:=payment.get_transaction_out_row(l_tr_out_row.con_id,pi_tr_out_id::varchar);
    l_des_op_row:=payment.add_operation_unique(l_des_tr_id,l_des_tr_out_row.id,l_des_op_tp_id,payment.get_status('work_accepting'),null::integer);
    perform payment.set_operation_status(l_des_op_row.id,payment.get_status('work_accepted'),null::integer,l_comment,localtimestamp);
    perform payment.set_transaction_out_status(l_des_tr_out_row.id,payment.get_status('work_accepted'));
    l_des_status_id:=payment.get_status('work_accepted');
    l_des_status_note:=null;
  exception
    when others then
      RAISE NOTICE 'ERROR : %',SQLERRM;
      l_des_status_id:=payment.get_status('work_denied');
      l_des_status_note:=format('Ошибка при зачисение на счет "%s": %s',l_acc_id,SQLERRM);
  end;

  perform payment.set_operation_status(l_op_row.id,l_des_status_id,null::integer,l_des_status_note,localtimestamp);
  perform payment.set_transaction_out_status(l_tr_out_row.id,l_des_status_id);

END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION account.create_payment_out(bigint,bigint,json)  OWNER TO acc_admin;