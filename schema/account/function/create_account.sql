﻿CREATE OR REPLACE FUNCTION account.create_account (
   pi_user_id uuid           /*Идентификатор*/
  ,pi_cur_id varchar = 'BAL' /*Валюта*/
  ,pi_amount bigint = 0      /*Начальная сумма*/
)
RETURNS integer AS
$body$
-- Функция по созданию счета
declare
    l_acc_id uuid;
begin
  perform from account.t_account  where user_id=pi_user_id;
  if found then
      raise notice 'Счет для пользователя (%) существует', pi_user_id;
      return global.get_status('no');
  end if;

  if pi_amount<0 then
      raise exception  'Отрицательная сумма (%) счета не допустима',pi_amount using HINT=global.get_status('param_check')::text;
  end if;

  l_acc_id:=uuid_generate_v4();
  insert into account.t_account(id,status_id,user_id,cur_id,amount,amount_reserved)
      values(l_acc_id,account.get_status('active'),pi_user_id,pi_cur_id,pi_amount,0);

  return global.get_status('ok');
end;

$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;


ALTER FUNCTION account.create_account(pi_user_id uuid, pi_cur_id varchar, pi_amount bigint)  OWNER TO acc_admin;