﻿CREATE OR REPLACE FUNCTION account.abandon_payment(
   pi_tr_id    bigint
  ,pi_is_admin boolean default false
)
RETURNS void as
$body$
-- Отмена пополение счета
/* в случае если */
DECLARE
  l_op_tp_code varchar:='abandon_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_tr_row      payment.t_transaction;
  l_op_row      payment.t_operation;
  l_acc_row    account.t_account;
BEGIN
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  -- блокировка счета
  l_acc_row:=account.get_account_row(l_tr_row.acc_id,true);
  if l_tr_row.direction::integer != 1 then
    raise exception 'Не допустимое направление транзакции для опреаци %',l_op_tp_code;
  end if;

  -- для одностадийной операции
  raise notice 'l_tr_row.acc_status_id:%, id:%',l_tr_row.acc_status_id,pi_tr_id;
  if l_tr_row.acc_status_id != payment.get_status('pay_abandoned') then
    l_op_row:=payment.add_operation_unique(pi_tr_id,l_op_tp_id,payment.get_status('pay_abandoning'),null::integer);
    if l_tr_row.acc_status_id in (payment.get_status('pay_accepted'),payment.get_status('pay_abandoning')) then
      perform account.sub_money(l_tr_row.acc_id,l_tr_row.amount,pi_is_admin);
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_abandoned'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_abandoned'));
      raise notice 'По счету % произошла отмена транзакции на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    else
      perform payment.set_operation_status(l_op_row.id,payment.get_status('pay_abandoned'),null::integer,null::varchar,localtimestamp);
      perform payment.set_transaction_status(pi_tr_id,payment.get_status('pay_abandoned'));
      raise notice 'По счету % произошла отмена транзакции на сумму %',l_tr_row.acc_id,l_tr_row.amount;
      return;
    end if;
  elsif l_tr_row.acc_status_id = payment.get_status('pay_abandoned') then
    raise notice 'Отмена транзакции % была уже произведена ',pi_tr_id;
    return;
  end if;

  raise exception 'Не предвиденая ошибка при отмене транзакции acc_status_id %',l_tr_row.acc_status_id;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION account.abandon_payment(bigint,boolean)  OWNER TO acc_admin;