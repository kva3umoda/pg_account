﻿CREATE OR REPLACE FUNCTION account.get_status
(
  in pi_code varchar
)
RETURNS account.td_status.id%type
AS
$body$
  /* Возвращает код статуса
  */
declare
  l_id  account.td_status.id%type;
begin
  select id
  into l_id
  from account.td_status
  where code=lower(pi_code);
  if NOT FOUND then
    raise exception 'Не верно указан код "%" статуса в account',lower(pi_code);
  end if;
  return l_id;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
RETURNS NULL ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION account.get_status(character varying)  OWNER TO acc_admin;

