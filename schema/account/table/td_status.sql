﻿CREATE TABLE account.td_status
(
   id         smallint
  ,code       varchar(64) NOT NULL
  ,name       text NOT NULL
  ,constraint pk_status primary key(id) using index tablespace ts_acc_idx
  ,constraint uk_status unique(code) using index tablespace ts_acc_idx
)
TABLESPACE ts_acc_dat;

ALTER TABLE account.td_status  OWNER TO acc_admin;

COMMENT ON TABLE account.td_status IS 'Cтатус счета пользователя';
COMMENT ON COLUMN account.td_status.id  IS 'Первичнй ключ';
COMMENT ON COLUMN account.td_status.code  IS 'Код статуса';
comment on column account.td_status.name is 'Наименование';

insert into account.td_status(id,code,name)
  values(1,'active','Активный')
       ,(2,'blocked','Заблокирован');
