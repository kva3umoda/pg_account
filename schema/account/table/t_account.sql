﻿create table account.t_account
(
   id               uuid
  ,status_id        smallint not null
  ,user_id          uuid    not null
  ,cur_id	          varchar(3) default 'BAL' not null
  ,amount	  	      integer default 0 not null
  ,amount_reserved	integer default 0 not null
  ,constraint pk_account primary key (id) using index tablespace ts_acc_idx
  ,constraint uk_account_usr unique(user_id) using index tablespace ts_acc_idx
)
tablespace ts_acc_dat;

ALTER TABLE account.t_account   OWNER TO acc_admin;

comment on table account.t_account                  is 'Счета пользователей';
comment on column account.t_account.id              is 'Идентификатор пользователя';
comment on column account.t_account.status_id 	    is 'Статус платежа';
comment on column account.t_account.user_id		      is 'Пользователь';
comment on column account.t_account.cur_id 	        is 'Валюта счета';
comment on column account.t_account.amount 	        is 'Сумма на счете';
comment on column account.t_account.amount_reserved is 'Размер резерва ожидающие конец операции';

alter table account.t_account add constraint fk_account_st foreign key(status_id) references account.td_status(id);
alter table account.t_account add constraint fk_account_cur foreign key(cur_id) references global.td_currency(id);
