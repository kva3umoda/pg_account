﻿/*Скрипы для драйвера ooopay.org*/
insert into payment.td_driver(code,name,class_name,is_async)
  values('ooopay','Платежный агрегатор ooopay.org','',true);

insert into payment.td_connection(name,driver_id,queue_code,direction,is_active)
  values('Соединение ooopay.org #1',payment.get_driver('ooopay'),'ooopay_1',1,TRUE);

insert into payment.t_con_pay_ch(con_id,pay_ch_id)
  values(payment.get_con_by_queue('ooopay_1'),payment.get_pay_channel('visa'));

insert into payment.t_con_pay_ch(con_id,pay_ch_id)
  values(payment.get_con_by_queue('ooopay_1'),payment.get_pay_channel('mastercard'));

insert into payment.t_con_pay_ch(con_id,pay_ch_id)
  values(payment.get_con_by_queue('ooopay_1'),payment.get_pay_channel('webmoney'));

insert into payment.t_driver_operation(driver_id,op_type_id,func_name_get,func_name_save)
  values(payment.get_driver('ooopay'),payment.get_operation_type('create_payment'),'dr_ooopay.get_create_payment','dr_ooopay.save_create_payment');  