﻿create table dr_ooopay.td_req_error
(
  check(driver_id=payment.get_driver('ooopay'))
)
inherits(payment.td_driver_error)
tablespace ts_global_dat;

ALTER TABLE dr_ooopay.td_req_error   OWNER TO acc_admin;

comment on table dr_ooopay.td_req_error is 'Ошибки запроса ooopay.org';

insert into dr_ooopay.td_req_error(driver_id,code,description,remote_code,is_continue,repeat_interval)
values(payment.get_driver('ooopay'),lower('account_disabled'),'Аккаунт отключен',lower('Account disabled'),false,0)
     ,(payment.get_driver('ooopay'),lower('sign_error'),'Подпись неверна',lower('sign error'),false,0)
     ,(payment.get_driver('ooopay'),lower('amount_error'),'Сумма неверна',lower('Amount error'),false,0)
     ,(payment.get_driver('ooopay'),lower('incorrect_WMID'),'WMID указан неверно',lower('Incorrect WMID'),false,0)
     ,(payment.get_driver('ooopay'),lower('merchant_not_found'),'Магазин не найден или отключен',lower('merchant not found or not activated'),false,0)
     ,(payment.get_driver('ooopay'),lower('need_verification'),'Необходимо пройти верификацию',lower('need verification'),false,0)
     ,(payment.get_driver('ooopay'),lower('low_amount'),'Недостаточно средств на балансе',lower('low amount'),true,60*60)
     ,(payment.get_driver('ooopay'),lower('service_not_found'),'Услуга не найдена',lower('Service not found'),false,0)
     ,(payment.get_driver('ooopay'),lower('user_not_found'),'Пользователь не найден',lower('user not found'),false,0)
     ,(payment.get_driver('ooopay'),lower('empty_payee'),'Не указан получатель',lower('empty payee'),false,0)
     ,(payment.get_driver('ooopay'),lower('payee_not_found'),'Получатель не найден',lower('payee not found'),false,0)
     ,(payment.get_driver('ooopay'),lower('can_not_pay_for_itself'),'Перевод самому себе запрещен',lower('can not pay for itself'),false,0)
     ,(payment.get_driver('ooopay'),lower('wrogn_currency'),'Валюта оплаты неверна',lower('wrogn currency'),false,0)
     ,(payment.get_driver('ooopay'),lower('order_not_found'),'Заказ не найден',lower('order not found'),false,0)
     ,(payment.get_driver('ooopay'),lower('order_exist'),' Такой номер заказа уже существует',lower('same order_id already exist'),false,0);


