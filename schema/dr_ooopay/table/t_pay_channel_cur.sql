﻿create table dr_ooopay.t_pay_channel_cur
(
   pay_ch_id integer
  ,cur_code   varchar(3)
  ,id        varchar(10) not null
  ,name      varchar  not null
  ,constraint pk_pay_channel_cur primary key (pay_ch_id,cur_code) using index tablespace ts_acc_idx
)
tablespace ts_acc_dat;

ALTER TABLE dr_ooopay.t_pay_channel_cur   OWNER TO acc_admin;

comment on table dr_ooopay.t_pay_channel_cur is 'Таблица зависимости от канала платежа и вылюты cur_id у ooopay.org';
comment on column dr_ooopay.t_pay_channel_cur.pay_ch_id is 'Канал платежа';
comment on column dr_ooopay.t_pay_channel_cur.cur_code   is 'Код валюты';
comment on column dr_ooopay.t_pay_channel_cur.id     is 'Код валюты  oopay.org';
comment on column dr_ooopay.t_pay_channel_cur.name   is 'Наименование';

alter table dr_ooopay.t_pay_channel_cur add constraint fk_pay_channel_cur foreign key(pay_ch_id) references payment.td_pay_channel(id);

insert into dr_ooopay.t_pay_channel_cur(pay_ch_id,cur_code,id,name)
  values(payment.get_pay_channel('webmoney'),'RUR','1','Webmoney WMR')
  ,(payment.get_pay_channel('webmoney'),'USD','2','Webmoney WMZ')
  ,(payment.get_pay_channel('qiwi'),'RUR','63','QIWI кошелек')
  ,(payment.get_pay_channel('yandexmoney'),'RUR','45','Яндекс.Деньги')
  ,(payment.get_pay_channel('visa'),'RUR','94','VISA')
  ,(payment.get_pay_channel('mastercard'),'RUR','94','MASTERCARD')
  ,(payment.get_pay_channel('paypal'),'RUR','70','PayPal')
  ;
/*
114 - Payeer RUR
115 - Payeer USD
109 - OOOPAY EUR
87 - OOOPAY USD
1 - Webmoney WMR
2 - Webmoney WMZ
63 - QIWI кошелек
45 - Яндекс.Деньги
73 - W1 USD
74 - W1 RUR
94 - VISA/MASTERCARD
70 - PayPal
*/