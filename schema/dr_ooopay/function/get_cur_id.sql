﻿CREATE OR REPLACE FUNCTION dr_ooopay.get_cur_id(
  in pi_pay_ch_id dr_ooopay.t_pay_channel_cur.pay_ch_id%type,
  in pi_cur_code  dr_ooopay.t_pay_channel_cur.cur_code%type
)
RETURNS dr_ooopay.t_pay_channel_cur.id%type
AS
$body$
declare
  l_id  dr_ooopay.t_pay_channel_cur.id%type;
begin
  select id
  into l_id
  from dr_ooopay.t_pay_channel_cur
  where pay_ch_id=lower(pi_pay_ch_id)
      and cur_code=pi_cur_code;
  if NOT FOUND then
    raise exception 'Не найден cur_id у подсистемы ooopay.org для канал "%" и валюты "%"',pi_pay_ch_id,pi_cur_code;
  end if;
  return l_id;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION dr_ooopay.get_cur_id(
  in pi_pay_ch_id dr_ooopay.t_pay_channel_cur.pay_ch_id%type,
  in pi_cur_code  dr_ooopay.t_pay_channel_cur.cur_code%type
)  OWNER TO acc_admin;