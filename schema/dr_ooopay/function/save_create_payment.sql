﻿CREATE OR REPLACE FUNCTION dr_ooopay.save_create_payment(
   pi_tr_id       bigint
  ,pi_tr_out_id   bigint
  ,pi_queue_code  varchar
  ,pi_event_id    integer
  ,pi_data        json
)
RETURNS integer AS
$body$
/*
  Сохранение результата ответа сервера
*/
DECLARE
  l_op_tp_code varchar:='create_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_op_row payment.t_operation;

  l_tr_row payment.t_transaction;
  l_tr_out_row payment.t_transaction_out;

  l_action varchar;
  l_req_code varchar;
  l_req_msg  varchar;
  l_tr_des_id varchar;
  l_pay_status varchar;

  l_err_row dr_ooopay.td_req_error;
BEGIN
  -- Блокировка транзакции
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  l_tr_out_row:=payment.get_transaction_out_row(pi_tr_out_id);
  l_op_row:=payment.get_operation_row(pi_tr_id,pi_tr_out_id,l_ot_tp_id);

  l_action:=pi_data->>'action';
  if l_tr_out_row.status_id=payment.get_status('work_accepting') then
    if l_action='cash_out' then

      l_req_code:=pi_data->>'error_code';
      l_req_msg:=pi_data->>'error_msg';
      if l_req_code='0' then
        update payment.t_transaction_out
        set tr_des_id=l_tr_des_id
        where id=pi_tr_out_id;
        perform queue.repeat_event(pi_queue_code,pi_event_id,60*15);
        return global.get_status('repeat');
      else
        l_err_row:=dr_ooopay.get_error_row(l_req_msg);
        if l_err_row.is_continue then
          perform queue.repeat_event(pi_queue_code,pi_event_id,l_err_row.repeat_interval);
          return global.get_status('repeat');
        end if;
        perform payment.set_operation_status(l_op_row.id,payment.get_status('work_denied'),null::integer,l_req_msg,localtimestamp);
        perform payment.set_transaction_status(pi_tr_id,payment.get_status('work_denied'));
        return global.get_status('ok');
      end if;
    elsif l_action='p_status' then
      l_req_code:=pi_data->>'error_code';
      l_req_msg:=pi_data->>'error_msg';
      if l_req_code='0' then
        l_pay_status:= pi_data->>'status';
        if l_pay_status='NEW' then
          perform queue.repeat_event(pi_queue_code,pi_event_id,60*15);
          return global.get_status('repeat');
        elsif l_pay_status='PAYED' then
          perform payment.set_operation_status(l_op_row.id,payment.get_status('work_accepted'),null::integer,null::varchar,localtimestamp);
          perform payment.set_transaction_status(pi_tr_id,payment.get_status('work_accepted'));
          return global.get_status('ok');
        elsif l_pay_status='COMPLETED' then
          perform payment.set_operation_status(l_op_row.id,payment.get_status('work_accepted'),null::integer,null::varchar,localtimestamp);
          perform payment.set_transaction_status(pi_tr_id,payment.get_status('work_accepted'));
          return global.get_status('ok');
        elsif l_pay_status='CANCELED' then
          l_op_row:=payment.add_operation_unique(pi_tr_id,payment.get_operation_type('abandon_payment'),payment.get_status('work_abandoning'),null::integer);
          perform payment.set_operation_status(l_op_row.id,payment.get_status('work_abandoned'),null::integer,'Удаленная система отменила платеж',localtimestamp);
          perform payment.set_transaction_status(pi_tr_id,payment.get_status('work_abandoned'));
          return global.get_status('ok');
        end if;
        raise exception 'Не предвиденная статус платежа status:%',l_pay_status;
      else
        l_err_row:=dr_ooopay.get_error_row(l_req_msg);
        if l_err_row.is_continue then
          perform queue.repeat_event(pi_queue_code,pi_event_id,l_err_row.repeat_interval);
          return global.get_status('repeat');
        end if;
        perform queue.repeat_event(pi_queue_code,pi_event_id,60*15);
        perform payment.set_operation_status(l_op_row.id,payment.get_status('work_accepting'),null::integer,l_req_msg,localtimestamp);
        return global.get_status('repeat');
      end if;
    end if;
  end if;

  raise exception 'Не предвиденная ошибка при создаие платежа';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION dr_ooopay.save_create_payment(
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_queue_code varchar
  ,pi_event_id   integer
  ,pi_data       json
) OWNER TO acc_admin;
