﻿CREATE OR REPLACE FUNCTION dr_ooopay.get_error_row(
  in pi_err_msg varchar
)
RETURNS dr_ooopay.td_req_error
AS
$body$
declare
  l_row  dr_ooopay.td_req_error;
begin
  select *
  into l_row
  from dr_ooopay.td_req_error
  where remote_code=lower(trim(pi_err_msg));
  return l_row;
end;
$body$
LANGUAGE 'plpgsql'
IMMUTABLE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 20;

ALTER FUNCTION dr_ooopay.get_error_row(
  in pi_err_msg varchar
)  OWNER TO acc_admin;