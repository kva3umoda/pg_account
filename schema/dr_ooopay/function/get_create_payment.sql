﻿CREATE OR REPLACE FUNCTION dr_ooopay.get_create_payment(
   pi_tr_id     bigint
  ,pi_tr_out_id bigint
  ,pi_queue_code varchar
  ,pi_event_id  integer
)
RETURNS json AS
$body$
/*
  Создание отве сервера
*/
DECLARE
  l_op_tp_code varchar:='create_payment';
  l_op_tp_id   integer:=payment.get_operation_type(l_op_tp_code);
  l_op_row payment.t_operation;

  l_tr_row payment.t_transaction;
  l_tr_out_row payment.t_transaction_out;

  l_action varchar:='cash_out';
  l_comment varchar;
  l_result json;
  l_to_account varchar;
  l_to_cur_id  varchar;
BEGIN
  -- Блокировка транзакции
  l_tr_row:=payment.get_transaction_row(pi_tr_id,true);
  l_tr_out_row:=payment.get_transaction_out_row(pi_tr_out_id);

  if l_tr_out_row.status_id=payment.get_status('work_new') then
    l_op_row:=payment.add_operation_unique(pi_tr_id,pi_tr_out_id,l_op_tp_id,payment.get_status('work_accepting'),pi_event_id);
    perform payment.set_transaction_status(pi_tr_id,payment.get_status('work_accepting'));
    /*C*/

    l_comment:=payment.get_tr_param(pi_tr_id,'comment');
    l_to_account:=payment.get_tr_param(pi_tr_id,pi_tr_out_id,'to_account');
    l_to_cur_id:=dr_ooopay.get_cur_id(l_tr_row.pay_ch_id,l_tr_out_row.cur_id);

    perform payment.save_tr_param(pi_tr_id,pi_tr_out_id,'action',l_action);
    perform payment.save_tr_param(pi_tr_id,pi_tr_out_id,'cur_id',l_to_cur_id);

      l_result:=json_build_object(
                 'order_id',l_tr_out_row.id
                ,'action','cash_out'
                ,'description',l_comment
                ,'amount',l_tr_out_row.amount
                ,'currency',l_tr_out_row.curr_id
                ,'to',l_to_account
                ,'cur_id',l_to_cur_id
                ,'func_result',global.get_status('ok')
                );
    return l_result;
  elsif l_tr_out_row.status_id=payment.get_status('work_acceting') then
    if l_tr_out_row.tr_des_id is null then
      l_comment:=payment.get_tr_param(pi_tr_id,'comment');
      l_to_account:=payment.get_tr_param(pi_tr_id,pi_tr_out_id,'to_account');
      l_to_cur_id:=payment.get_tr_param(pi_tr_id,pi_tr_out_id,'cur_id');
      l_result:=json_build_object(
                 'order_id',l_tr_out_row.id
                ,'action','cash_out'
                ,'description',l_comment
                ,'amount',l_tr_out_row.amount
                ,'currency',l_tr_out_row.curr_id
                ,'to',l_to_account
                ,'cur_id',l_to_cur_id
                ,'func_result',global.get_status('ok')
                );
      return l_result;
    else
      l_result:=json_build_object(
                 'order_id',l_tr_out_row.id
                ,'action','p_status'
                ,'id',l_tr_out_row.tr_des_id
                ,'direction',3/*Вывод средств*/
                ,'func_result',global.get_status('ok')
                );
      return l_result;
    end if;
  else
    l_result:=json_build_object(
                'func_result',global.get_status('not_action')
                );
  end if;

  raise exception 'Не предвиденная ошибка при создаие платежа';
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY DEFINER
COST 1;

ALTER FUNCTION dr_ooopay.get_create_payment(bigint,bigint,varchar, integer) OWNER TO acc_admin;