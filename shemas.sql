﻿CREATE SCHEMA queue AUTHORIZATION acc_admin;
CREATE SCHEMA account AUTHORIZATION acc_admin;
CREATE SCHEMA payment AUTHORIZATION acc_admin;
CREATE SCHEMA global AUTHORIZATION acc_admin;

CREATE SCHEMA dr_ooopay   AUTHORIZATION acc_admin; -- Схема драйвера.
CREATE SCHEMA web_payment AUTHORIZATION acc_admin; -- Схема для создания платежа